﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public struct Options {

	#region InspectorFields

	[Header("Volume Options")]
	[Range(0,1)] 
	[SerializeField]
	private float volumeFX;
	[Range(0,1)] 
	[SerializeField]
	private float volumeMusic;
	[Range(0,1)] 
	[SerializeField]
	private float volumeAmbient;

	[Header("Other Local Settings")]
	[SerializeField]
	private bool isEnergySaver; //Energy saver should ping GPS less often. 

	#endregion

	#region Properties

	public float VolumeFX {
		get { return volumeFX; }
		set {
			volumeFX = value;

			//Ensure the volume for the FX is in range
			volumeFX = Mathf.Clamp (volumeFX, 0, 1);
		}
	}

	public float VolumeMusic {
		get { return volumeMusic; }
		set {
			volumeMusic = value;

			//Ensure the volume for the Music is in range
			volumeMusic = Mathf.Clamp (volumeMusic, 0, 1);
		}
	}

	public float VolumeAmbient {
		get { return volumeAmbient; }
		set {
			volumeAmbient = value;

			//Ensure the volume for the Ambience is in range
			volumeAmbient = Mathf.Clamp (volumeAmbient, 0, 1);
		}
	}

	public bool IsEnergySaver {
		get { return isEnergySaver; }
		set { isEnergySaver = value; }
	}

	#endregion
}
