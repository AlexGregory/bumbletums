﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollViewScaler : MonoBehaviour {
	float numberOfItems;
	RectTransform Panel;
	GridLayoutGroup grid;

	// Use this for initialization
	void Start () {
		Panel = GetComponent<RectTransform> ();
		grid = GetComponent<GridLayoutGroup> ();
	}
	
	// Update is called once per frame
	void Update () {
		numberOfItems = Panel.gameObject.transform.childCount;
		Panel.sizeDelta = new Vector2 (Panel.sizeDelta.x, (grid.cellSize.y * numberOfItems / grid.constraintCount) + grid.cellSize.y);
	}
}
