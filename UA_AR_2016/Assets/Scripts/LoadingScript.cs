﻿using UnityEngine;
using System.Collections;

public class LoadingScript : MonoBehaviour {
	// Update is called once per frame
	void Start () {
		StartCoroutine (DoLogin ());
	}


	public IEnumerator DoLogin () {

		//TODO: Change scene to show that we are logging in

		// Either log in them in (and set their token), or create a new account
		yield return StartCoroutine (DataManager.instance.DoPlayerLogin());

		//TODO: Change scene to show that we are loading player data
		yield return StartCoroutine (DataManager.instance.DoPlayerLoad());

		// TODO: Check for GPS Data OR Error out


		// TODO: Change scene to say it is loading world data

		// Load world data
		yield return StartCoroutine (DataManager.instance.DoWorldLoad(GameManager.instance.playerLocation));

		// Wait for one fade to finish
		yield return new WaitForSeconds (GameManager.instance.fadeSpeed);

		// Then load the main scene
		Debug.Log ("LOADING MAIN SCENE START");
		GameManager.instance.LoadScene ("MainScene");
		Debug.Log ("LOADING MAIN SCENE END");
	}
}
