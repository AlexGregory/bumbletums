﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class LogoTimer : MonoBehaviour {

	public float waitTime = 0.0f;
	public string SceneName = " ";
	private bool loadStarted = false;

	void Update(){
		if (loadStarted) {
			return;
		}

		waitTime -= Time.deltaTime;
		if(waitTime <= 0.0f){
			GameManager.instance.LoadScene (SceneName);
			loadStarted = true;
		}
	}
}
