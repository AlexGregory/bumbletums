﻿using UnityEngine;
using System;
using System.Collections;

/********** ENUMS **************/
public enum WorldObjectType {Creature = 0, Beacon = 1, Food = 2}; 

public class Helpers {
	/****************** CONVERSIONS BETWEEN INVENTORY ITEMS AND WORLD ITEMS **********************************************************************/

	/// <summary>
	/// Search through the List in the DataManager and return the Actor from that list
	/// </summary>
	/// <returns>The Actor from the DataManager list.</returns>
	/// <param name="id">ID of the Actor you want to find.</param>
	public static ActorCreature FindCreatureFromID (string id) {
		for (int i = 0; i < DataManager.instance.CreatureList.Length; i++) {
			if (id == DataManager.instance.CreatureList [i].id) {
				return DataManager.instance.CreatureList [i];
			}
		}
		return null;
	}

	/// <summary>
	/// Search through the List in the DataManager and return the Actor from that list
	/// </summary>
	/// <returns>The Actor from the DataManager list.</returns>
	/// <param name="id">ID of the Actor you want to find.</param>
	public static ActorBeacon FindBeaconFromID (string id) {
		for (int i = 0; i < DataManager.instance.BeaconList.Length; i++) {
			if (id == DataManager.instance.BeaconList [i].id) {
				return DataManager.instance.BeaconList [i];
			}
		}
		return null;
	}

	/// <summary>
	/// Search through the List in the DataManager and return the Actor from that list
	/// </summary>
	/// <returns>The Actor from the DataManager list.</returns>
	/// <param name="id">ID of the Actor you want to find.</param>
	public static ActorFood FindFoodFromID (string id) {
		for (int i = 0; i < DataManager.instance.FoodList.Length; i++) {
			if (id == DataManager.instance.FoodList [i].id) {
				return DataManager.instance.FoodList [i];
			}
		}
		return null;
	}

	/******************* CONVERSIONS ************************/

	// Converts percents (0 to 1) to degrees (0 to 360)
	/// <summary>
	/// Converts Percents (0 to 1) to degrees (0 to 360).
	/// </summary>
	/// <returns>The degrees (0 to 360)</returns>
	/// <param name="percent">The Percent (0 to 1)</param>
	public static float Percent01ToDegrees ( float percent ) {
		return percent * 360.0f;
	}

	// Converts degrees (0 to 360) to percents (0 to 1)
	/// <summary>
	/// Convert Degrees to a percent between 0 and 1.
	/// </summary>
	/// <returns>Percent value (0 to 1)</returns>
	/// <param name="degrees">Degrees (0 to 360) to convert.</param>
	public static float DegreesToPercent01 (float degrees ) {
		return (degrees / 360.0f);
	}


	// Write function to turn GPS location to world location
	public static Vector3 GPSLocationToWorldLocation ( GPSLocation location ) {

		Vector3 temp = Helpers.ObjectCoordinates (GameManager.instance.playerLocation.latitude, location.latitude, GameManager.instance.playerLocation.longitude, location.longitude);
		return temp;
	}

	// TODO: WorldLocationToGPSLocation
	public static GPSLocation WorldLocationToGPSLocation ( Vector3 worldLocation ) {

		// GameManager.instance.playerLocation <-- will be an important part of this
		// Player's world location should always be (0,0,0);

		GPSLocation temp = new GPSLocation();
		return temp;
	}

	public static DateTime TimestampToDateTime ( double timestamp ) {
		// Start with the unix epoch
		DateTime epoch = new DateTime (1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc); 
		// add the seconds we passed in
		epoch = epoch.AddSeconds (timestamp);
		// return that datetime object
		return epoch;
	}

	public static double DateTimeToTimestamp ( DateTime input ) {
		// Save the unix epoch
		DateTime epoch = new DateTime (1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc); 

		// return total seconds in difference between time passed in and epoch
		TimeSpan timeSinceEpoch = input.ToUniversalTime() - epoch;

		// return number of seconds since epoch
		return timeSinceEpoch.TotalSeconds;
	}

	public static double StringTimeToTimestamp ( string input ) {
		DateTime temp = StringToTimeObject (input); 
		return DateTimeToTimestamp (temp);
	}



	public static GPSLocation RandomLocationInRangeGPS ( float range ) {

		// Get a random point near the player
		Vector3 spawnLocation;  

		double newX = GameManager.instance.playerLocation.longitude + UnityEngine.Random.Range( -1*range, range);
		double newY = GameManager.instance.playerLocation.latitude + UnityEngine.Random.Range( -1*range, range);

		// Note: Change to ignore altitude
		double newZ = 0.0;
		// double newZ = GameManager.instance.playerLocation.altitude;

		spawnLocation = new Vector3 ((float)newX, (float)newY, (float)newZ);

		Debug.Log ("Spawning: Spawn Location = " + spawnLocation);

		// Turn it back into a GPSLocation (With same altitude as player)
		GPSLocation spawnGPSLocation = new GPSLocation();
		spawnGPSLocation.latitude = spawnLocation.y;
		spawnGPSLocation.longitude = spawnLocation.x;
		spawnGPSLocation.altitude = GameManager.instance.playerLocation.altitude;

		return spawnGPSLocation;
	}


	/// <summary>
	/// Convert String Time to time object.
	/// </summary>
	/// <returns>The DateTime object.</returns>
	/// <param name="input">Input.</param>
    public static DateTime StringToTimeObject(string input) // takes a mysql timestamp and converts it into a DateTime object
    {
        DateTime dateObject = new DateTime(); // instantiates the variable
        dateObject = DateTime.Parse(input); // uses the DateTime.Parse .net function that parses the inputted string to makes it into a DateTime function
        return (dateObject); // returns the datetime object
    }

	/// <summary>
	/// Convert DateTime the to string.
	/// </summary>
	/// <returns>The String time.</returns>
	/// <param name="input">Input.</param>
    public static String DateTimeToString(DateTime input) // takes in a DateTime object and converts it into a string
    {
        string strTimestamp = ""; // instantiates the string that will be returned
        strTimestamp += (int)input.Month; // takes in the specific time area we are looking for to add into the string
        strTimestamp += "/"; // adds in a special character
        strTimestamp += (int)input.Day; // repeats  until the return
        strTimestamp += "/";
        strTimestamp += (int)input.Year;
        strTimestamp += " ";
        strTimestamp += (int)input.Hour;
        strTimestamp += ":";
        strTimestamp += (int)input.Minute;
        strTimestamp += ":";
        strTimestamp += (int)input.Second;
        return strTimestamp; // returns the string
    }


	/************************************* GPS SCRIPTS FROM GPS CLASS ***************************************/
	private static double GPSTest(double lat1, double lat2, double lon1, double lon2)
	{
		double R = 6372797.560856f; //6372797.560856f
		double latOneRadians = ((lat1 / 180) * Mathf.PI);
		double latTwoRadians = ((lat2 / 180) * Mathf.PI);
		double latDistance = ((lat2 - lat1) / 180 * Mathf.PI);
		double lonDistance = ((lon2 - lon1) / 180 * Mathf.PI);

		double a = (Mathf.Sin ((float)latDistance / 2) * Mathf.Sin ((float)latDistance / 2) + Mathf.Sin ((float)lonDistance / 2) * Mathf.Sin ((float)lonDistance / 2) * Mathf.Cos ((float)latOneRadians) * Mathf.Cos ((float)latTwoRadians));
		double c = 2 * Mathf.Asin (Mathf.Sqrt((float)a));

		return R * c;
	}

	private static double BearingTest(double lat1, double lat2, double lon1, double lon2)
	{

		//double R = 6371000;
		double latOneRadians = ((lat1 / 180) * Mathf.PI);
		double latTwoRadians = ((lat2 / 180) * Mathf.PI);
		//double latDistance = ((lat2 - lat1) / 180 * Mathf.PI);
		double lonDistance = ((lon2 - lon1) / 180 * Mathf.PI);

		double x = Mathf.Sin((float)lonDistance) * Mathf.Cos((float)latTwoRadians);
		double y = Mathf.Cos((float)latOneRadians) * Mathf.Sin((float)latTwoRadians) - Mathf.Sin((float)latOneRadians) * Mathf.Cos((float)latTwoRadians) * Mathf.Cos((float)lonDistance);
		float b = (Mathf.Atan2((float)x,(float)y) * 180 / Mathf.PI) ;
		return ( b + 360 ) % 360;
	}

	public static Vector3 ObjectCoordinates(double playerLat, double objectLat, double playerLon, double objectLon)

	{
		double distance = GPSTest (playerLat, objectLat, playerLon, objectLon);

		double angle = BearingTest (playerLat, objectLat, playerLon, objectLon);
		angle = angle * Mathf.Deg2Rad;
		double northSouth = Mathf.Cos ((float)angle) * distance;
		double eastWest = Mathf.Sin ((float)angle) * distance;

		//Debug.Log (northSouth);
		//Debug.Log (eastWest);
		Vector3 objectLocation = new Vector3 ((float)eastWest, 0, (float)northSouth);
		return objectLocation;
	}


	/// <summary>
	/// Returns the first four characters of a string (maybe an error message?)
	/// </summary>
	/// <returns>The four.</returns>
	/// <param name="input">Input.</param>
	public static string FirstFour ( string input ) {
		if (input != null && input.Length > 4) {
			return input.Substring (0, 4);
		}
		return input;
	}

}