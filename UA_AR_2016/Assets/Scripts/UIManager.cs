﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public void ChangeMenuState(GameObject menuObject){
		menuObject.SetActive (!menuObject.activeSelf);
	}
		
	public void LoadScene(string sceneToLoad){
		GameManager.instance.LoadScene (sceneToLoad);
	}

	public void OpenCreatureInventory() {
		GameManager.instance.LoadScene ("Inventory");
		GameManager.instance.inventory = GameManager.instance.playerData.creatureInv;
	}

	public void OpenBeaconInventory() {
		GameManager.instance.LoadScene ("Inventory");
		GameManager.instance.inventory = GameManager.instance.playerData.beaconInv;
	}

	public void OpenFoodInventory() {
		GameManager.instance.LoadScene ("Inventory");
		GameManager.instance.inventory = GameManager.instance.playerData.foodInv;
	}
		
	public void purchasePart1(GameObject purchaseVerify){
		purchaseVerify.SetActive (true);
	}

	public void purchasePart2(GameObject purchaseVerify){
		purchaseVerify.SetActive (false);
	}

	public void denyPurchase(GameObject purchaseVerify){
		purchaseVerify.SetActive (false);
	}

}
