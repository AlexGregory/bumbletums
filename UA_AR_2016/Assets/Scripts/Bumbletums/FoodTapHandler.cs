﻿using UnityEngine;
using System.Collections;

public class FoodTapHandler : MonoBehaviour {

	ActorFood actor;	// This actor
	WorldDrawer worldDrawer; // The world drawer, so we can redraw


	// Use this for initialization
	void Start () {
		if (worldDrawer == null) {
			worldDrawer = GetComponent<WorldDrawer> ();
		}

		if (worldDrawer == null) {
			worldDrawer = FindObjectOfType <WorldDrawer> ();
		}

		if (worldDrawer == null) {
			Debug.LogError("ERROR: Could not find World Drawer");
		}

		if (actor == null) {
			actor = GetComponent<ActorFood> ();
		}

		if (worldDrawer == null) {
			Debug.LogError("ERROR: Could not find Actor");
		}
	}

	// Update is called once per frame
	void Update () {
	
	}


	void OnMouseDown() {

		// Don't let clicks happen if paused
		if (GameManager.instance.isPaused)
			return;

		StartCoroutine (PickupFood ());
	}


	IEnumerator PickupFood () {
		// Pause Clicks Only
		GameManager.instance.isPaused = true;

		// Show food pickup particle
		Vector3 particleLocation = new Vector3(transform.position.x, transform.position.y + DataManager.instance.particleOffset.y, transform.position.z);
		Instantiate (DataManager.instance.eatParticlePrefab, particleLocation, Quaternion.identity);

		// Add to inventory
		Debug.Log("Adding "+actor.instanceData.UID+" to inventory.");
		GameManager.instance.playerData.foodInv.AddItem(actor.instanceData);

		// Save player
		yield return StartCoroutine(DataManager.instance.DoPlayerSave());

		// Redraw world
		worldDrawer.ResetWorld();

		// UnPause
		GameManager.instance.isPaused = false;
	}


}
