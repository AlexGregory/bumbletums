﻿using UnityEngine;
using System.Collections;

public class FeedOnTap : MonoBehaviour {

	public ArMode arMode;
	private ActorFood food;

	public void Start () {
		food = GetComponent<ActorFood> ();
	}

	public void OnMouseDown () {

		if (food == null) {
			Debug.Log ("ERROR: This food has no stats");
			return;
		}

		// Pause
		GameManager.instance.Paused = true;

		// Debug
		Debug.Log ("Feeding " + gameObject.name);

		// Start the actual feeding
		StartCoroutine(DoFeed());
	}

	public IEnumerator DoFeed () {
		
		// TODO: Play a feeding sound! Yay!

		// Instantiate the particles
		arMode.ShowEatParticles();

		// Change stats
		arMode.catchChance += food.catchChance.value;
		arMode.runChance -= food.fleeChance.value;

		// Update the chance values
		arMode.UpdateChanceValues ();

		// Remove from inventory
		GameManager.instance.playerData.foodInv.RemoveItem(food.instanceData);

		// If we still have food - otherwise turn off bar
		if (arMode.foodController.CheckBarDisplay()) {			
			// Change index 
			arMode.foodController.foodIndex--;
			Mathf.Clamp (arMode.foodController.foodIndex, 0, GameManager.instance.playerData.foodInv.ItemCount);

			// Update food
			arMode.UpdateFood ();
		}

		// unpause
		GameManager.instance.Paused = false;

		yield return null;
	}

}
