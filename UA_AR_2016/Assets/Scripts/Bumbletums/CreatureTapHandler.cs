﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CreatureTapHandler : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown() {
		if (SceneManager.GetActiveScene ().name == "ArMode") {
			// Do Nothing - we have a return button for getting back
			// We also return on capture and escape
		} else {
			//Make this creature persistant so it can be used in the Ar Mode
			GameManager.instance.selectedCreature = GetComponent<ActorCreature>().instanceData;

			//Load the Ar Mode Scene
			GameManager.instance.LoadScene ("ArMode");
		}
	}
}
