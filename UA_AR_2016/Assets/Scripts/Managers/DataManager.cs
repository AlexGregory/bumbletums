﻿/**********************************************************************
 * Class: DataManager
 * Purpose: Load and Save Data to the servers
 * Notes: Use the singleton to access functions -- DataManager.instance.whatever - Hue
 * *********************************************************************/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class DataManager : MonoBehaviour {

	//Singleton Instance
	public static DataManager instance = null;

	// Web URLS
	[Header("Web URLs")]
	public string loginWebURL = "http://www.devilloper.com/bumbletums/login.php";
	public string WorldObjectLoadWebURL = "http://www.devilloper.com/bumbletums/loadWorldData.php";
	public string PlayerDataLoadWebURL = "http://www.devilloper.com/bumbletums/playerData.php";
	public string WorldObjectSaveWebURL = "http://www.devilloper.com/bumbletums/saveWorldData.php";
	public string PlayerDataSaveWebURL = "http://www.devilloper.com/bumbletums/savePlayerData.php";

	// Lists of all the resources -- we can pull data from these to create new bumbletums for the player's data
	[Header("Object Lists")]
	public ActorCreature[] CreatureList;
	public ActorFood[] FoodList;
	public ActorBeacon[] BeaconList;

	// Prefabs
	[Header("Particle Effects")]
	public GameObject successParticlePrefab;
	public GameObject failParticlePrefab;
	public GameObject eatParticlePrefab;
	public Vector3 particleOffset; // Because particles aren't at the creature's feet!

	// How long until a web connection times out
	public float timeOutTime = 10.0f;

	// Data for saving and loading
	[TextArea(3,100)]
	public string inputOutputJSON;

	// Use this for initialization
	void Awake () {
		// Create Singleton 
		// Debug.Log("Singleton Created.");
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
	}

	public void LoadResources () {
		CreatureList = Resources.LoadAll<ActorCreature>("Bumbletums/");
		FoodList = Resources.LoadAll<ActorFood>("Foods/");
		BeaconList = Resources.LoadAll<ActorBeacon>("Beacons/");
	}

	/*********************************************************************** LOGIN ****************************************************************************/

	/***************************************************
	* Function: LoginOrCreate
	* Purpose: Updates player token or creates a new player (with token)
	* Inputs: none
	* Returns: None
	* *************************************************/
	public void LoginOrCreate () {
		// Call the coroutine to actually save
		StartCoroutine("DoPlayerLogin");
	}

	/***************************************************
	 * Coroutine to login
	***************************************************/
	public IEnumerator DoPlayerLogin () {
		Debug.Log ("Start Login/Token Update");
		PlayerData playerData = GameManager.instance.playerData;

		GameManager.instance.playerData.userid = GameManager.instance.playerData.userid.Replace ("\"", "");

		// Clear out object
		DataManager.TempPlayerLoginData playerDataForLogin = new DataManager.TempPlayerLoginData ();
		playerDataForLogin.userid = playerData.userid;
		playerDataForLogin.token = playerData.token;

		// Make it a JSON
		inputOutputJSON = JsonUtility.ToJson(playerDataForLogin);

		// Save the time we started trying to load
		float startTime = Time.time;

		// Pause the game
		GameManager.instance.Paused = true;

		// Put the JSON into the only form element
		WWWForm form = new WWWForm();
		form.AddField("json", inputOutputJSON);

		// Start loading the webpage
		//PlayerDataWebURL = WWW.EscapeURL (PlayerDataWebURL);
		WWW www = new WWW (loginWebURL, form);

		// TODO: Start showing the loading icon

		// While it is not finished loading
		while (!www.isDone) {
			// If we have timed out, quit our coroutine
			if (Time.time > startTime + timeOutTime) {
				// Unpause
				GameManager.instance.Paused = false;

				// Do what we do on a fail
				OnPlayerLoadFail ();

				// Quit coroutine
				yield break;
			}

			// TODO: Update the spinner / loading graphic
			// use www.progress -- this  gives a value 0 to 1 on what percent is complete!

			// Draw next frame
			yield return null;
		}

		// Save our response where we can read it in the inspector
		inputOutputJSON = www.text;

		// If we got an error, process it.
		if (!ProcessError (www.text)) {
			yield break;
		}

		OnPlayerLoginSuccess ();
	}


	/***************************************************
	 * Function: OnPlayerLoginFail
	 * Purpose: Event that runs when the player fails to load
	 * Inputs: None
	 * Returns: None
	 * *************************************************/
	private void OnPlayerLoginFail() {
		Debug.Log ("Player login fail");
	}

	/***************************************************
	 * Function: OnPlayerLoginSuccess
	 * Purpose: Event that runs when the player loads
	 * Inputs: None
	 * Returns: None
	 * *************************************************/
	private void OnPlayerLoginSuccess () {
		Debug.Log ("Player login success");
		GameManager.instance.Paused = false;
	}


/*********************************************************************** SAVE PLAYER ****************************************************************************/


	/***************************************************
	 * Function: SavePlayerData
	 * Purpose: Saves player data from the web forms
	 * Inputs: none
	 * Returns: None
	 * *************************************************/
	public void SavePlayerData () {

	// Call the coroutine to actually save
		StartCoroutine("DoPlayerSave");
	}

	public IEnumerator DoPlayerSave () {
		Debug.Log ("Start Saving Player");
		GameManager.instance.playerData.userid = GameManager.instance.playerData.userid.Replace ("\"", "");


		//Grab the player data reference from game manager
		PlayerData playerData = GameManager.instance.playerData;

		TempPlayerSaveData playerDataToSave = new TempPlayerSaveData();

		// Update the data fields
		playerDataToSave.userid = GameManager.instance.playerData.userid;
		playerDataToSave.token = GameManager.instance.playerData.token;
		playerDataToSave.displayName = GameManager.instance.playerData.displayName; 
		playerDataToSave.coins = GameManager.instance.playerData.coins;
		playerDataToSave.gems = GameManager.instance.playerData.gems;
		if (GameManager.instance.playerData.currentAvatar != null) {
			playerDataToSave.avatarid = GameManager.instance.playerData.currentAvatar.id;
		}
		playerDataToSave.lastSpawnTimestamp = GameManager.instance.playerData.lastSpawnTimestamp.ToString();

		// Clear lists
		playerDataToSave.beacons = new List<TempObject> ();
		playerDataToSave.bumbletums = new List<TempObject> ();
		playerDataToSave.food = new List<TempObject> ();

		//Create the save objects for the beacons, food, and bumbletums
		playerDataToSave.beacons = CreateTempObjectsFromInventory (playerData.beaconInv);
		playerDataToSave.food = CreateTempObjectsFromInventory (playerData.foodInv);
		playerDataToSave.bumbletums = CreateTempObjectsFromInventory (playerData.creatureInv);
		playerDataToSave.graveyard = GameManager.instance.playerData.graveyard.objects;

		// Now that our data is stored in playerDataToSave, turn it into a JSON 
		inputOutputJSON = JsonUtility.ToJson(playerDataToSave);

		// Save the time we started trying to load
		float startTime = Time.time;

		// Pause the game
		GameManager.instance.Paused = true;

		// Put the JSON into the only form element
		WWWForm form = new WWWForm();
		form.AddField("json", inputOutputJSON);

		// Start loading the webpage
		WWW www = new WWW (PlayerDataSaveWebURL, form);

		// TODO: Start showing the loading icon

		// While it is not finished loading
		while (!www.isDone) {
			// If we have timed out, quit our coroutine
			if (Time.time > startTime + timeOutTime) {
				// Unpause
				GameManager.instance.Paused = false;

				// Do what we do on a fail
				OnPlayerSaveFail ();

				// Quit coroutine
				yield break;
			}

			// TODO: Update the spinner / loading graphic
			// use www.progress -- this  gives a value 0 to 1 on what percent is complete!

			// Draw next frame
			yield return null;
		}

		// Save our response where we can read it in the inspector
		inputOutputJSON = www.text;

		// If we got an error, process it.
		if (!ProcessError (www.text)) {
			yield break;
		}

		// Unpause
		GameManager.instance.Paused = false;

		// Do what we do after we save
		OnPlayerSaveSuccess();
	}

	/***************************************************
	 * Function: OnPlayerSaveFail
	 * Purpose: Event that runs when the player fails to load
	 * Inputs: None
	 * Returns: None
	 * *************************************************/
	private void OnPlayerSaveFail() {
		Debug.Log ("Player save fail");
	}

	/***************************************************
	 * Function: OnPlayerLoadSuccess
	 * Purpose: Event that runs when the player loads
	 * Inputs: None
	 * Returns: None
	 * *************************************************/
	private void OnPlayerSaveSuccess () {
		Debug.Log ("Player save success");
	}

/*********************************************************************** LOAD PLAYER ****************************************************************************/

	/***************************************************
	 * Function: LoadPlayerData
	 * Purpose: Loads player data from the web database
	 * Inputs: None
	 * Returns: None
	 * *************************************************/
	public void LoadPlayerData () {
		// Actually do the load
		StartCoroutine("DoPlayerLoad");
	}


	/***************************************************
	 * DO THE LOADING OF THE PLAYER 
	 *************************************************/
	public IEnumerator DoPlayerLoad ( ) {
		Debug.Log ("Start Loading Player");

		GameManager.instance.playerData.userid = GameManager.instance.playerData.userid.Replace ("\"", "");

		// Put the player's userID and token into the JSON object
		inputOutputJSON = "{\"userid\": \""+GameManager.instance.playerData.userid+"\", \"token\": \""+GameManager.instance.playerData.token+"\"}";

		// Save the time we started trying to load
		float startTime = Time.time;

		// Pause the game
		GameManager.instance.Paused = true;

		// Put the JSON into the only form element
		WWWForm form = new WWWForm();
		form.AddField("json", inputOutputJSON);


		// Start loading the webpage
		WWW www = new WWW (PlayerDataLoadWebURL, form);

		// TODO: Start showing the loading icon

		// While it is not finished loading
		while (!www.isDone) {
			// If we have timed out, quit our coroutine
			if (Time.time > startTime + timeOutTime) {
				// Unpause
				GameManager.instance.Paused = false;

				// Do what we do on a fail
				OnPlayerLoadFail ();

				// Quit coroutine
				yield break;
			}

			// TODO: Update the spinner / loading graphic
			// use www.progress -- this  gives a value 0 to 1 on what percent is complete!

			// Draw next frame
			yield return null;
		}

		// If load failed due to no internet
		if (www.error != null) {
			Debug.LogError ("ERROR: Could  not load Player Data - " + www.error);
			GameManager.instance.LoadScene ("NoGPS");
		}

		// Save our response where we can read it in the inspector
		inputOutputJSON = www.text;

		// Unpause
		GameManager.instance.Paused = false;

		// Check for errors and change scenes if needed
		if (!ProcessError (www.text)) {
			yield break;
		}

		// Successful load

		// COPY PLAYER DATA INTO THE GAMEMANAGER
		// Convert the www text into our object
		TempPlayerLoadData tempPlayerData;
		tempPlayerData = JsonUtility.FromJson<TempPlayerLoadData> (inputOutputJSON);

		// Move everything from our temp saving/loading structure to our actual player data
		PlayerData pd = GameManager.instance.playerData;

		pd.coins = tempPlayerData.coins;
		pd.gems = tempPlayerData.gems;
		pd.userid = tempPlayerData.userid;
		pd.displayName = tempPlayerData.displayName;
		if (tempPlayerData.avatarid != null) {
			pd.currentAvatar = Helpers.FindCreatureFromID (tempPlayerData.avatarid);
		}
		if (!double.TryParse(tempPlayerData.lastSpawnTimestamp, out GameManager.instance.playerData.lastSpawnTimestamp)) { 
			GameManager.instance.playerData.lastSpawnTimestamp = 0.0;
		}
			

		//Convert the data from the server to inventory items for the player to use
		pd.creatureInv.Clear ();
		pd.foodInv.Clear ();
		pd.beaconInv.Clear ();

		pd.creatureInv.AddItems ( TempObjectsToInstanceCreatures ( tempPlayerData.bumbletums ) );
		pd.beaconInv.AddItems ( TempObjectsToInstanceBeacons ( tempPlayerData.beacons ) );
		pd.foodInv.AddItems ( TempObjectsToInstanceFoods ( tempPlayerData.food ));
		pd.graveyard.objects = tempPlayerData.graveyard;

		OnPlayerLoadSuccess();
	}

	/***************************************************
	 * Function: OnPlayerLoadFail
	 * Purpose: Event that runs when the player fails to load
	 * Inputs: None
	 * Returns: None
	 * *************************************************/
		private void OnPlayerLoadFail() {
		Debug.Log ("Player load fail");
	}

	/***************************************************
	 * Function: OnPlayerLoadSuccess
	 * Purpose: Event that runs when the player loads
	 * Inputs: None
	 * Returns: None
	 * *************************************************/
		private void OnPlayerLoadSuccess () {
		Debug.Log ("Player load success");
	}

/*********************************************************************** SAVE BEACON ****************************************************************************/
	/***************************************************
	* Function: SaveWorldObject
	* Purpose: Saves a world object to the database
	* Inputs: world object
	* Returns: 
	* *************************************************/
	public void SaveBeacon (InstanceBeacon obj) {
		// Get the info from the PHP page based on this data
		StartCoroutine("DoBeaconSave", obj);
	}
	public void SaveFood (InstanceFood obj) {
		// Get the info from the PHP page based on this data
		StartCoroutine("DoFoodSave", obj);
	}
	public void SaveCreature (InstanceCreature obj) {
		// Get the info from the PHP page based on this data
		StartCoroutine("DoCreatureSave", obj);
	}


	/***************************************************
	 * SAVE THE WORLD OBJECT
	 *************************************************/

	public IEnumerator DoCreatureSave (InstanceCreature obj) {
		Debug.Log ("Start Saving Creature: "  + obj.UID + " | " + obj.sharedData.id );

		// Put the player's userID and token into the JSON object
		TempWorldObjectSaveData tempObject = new TempWorldObjectSaveData();
		tempObject.userid = GameManager.instance.playerData.userid;
		tempObject.token = GameManager.instance.playerData.token;

		// Set the variables
		tempObject.uid = obj.UID;
		tempObject.clientid = obj.sharedData.id;
		tempObject.lifespan = obj.sharedData.lifespan.value;
		tempObject.latitude = obj.location.latitude;
		tempObject.longitude = obj.location.longitude;
		// Note: Changing to ignore altitude -- always at 0
		tempObject.altitude = 0.0f;
		// tempObject.altitude = obj.location.altitude;
		tempObject.type = "creature";

		// Set the JSON for saving
		inputOutputJSON = JsonUtility.ToJson (tempObject);

		// Save the time we started trying to load
		float startTime = Time.time;

		// Pause the game
		GameManager.instance.Paused = true;

		// Put the JSON into the only form element
		WWWForm form = new WWWForm();
		form.AddField("json", inputOutputJSON);

		// Start loading the webpage
		WWW www = new WWW (WorldObjectSaveWebURL, form);

		// TODO: Start showing the loading icon

		// While it is not finished saving
		while (!www.isDone) {
			// If we have timed out, quit our coroutine
			if (Time.time > startTime + timeOutTime) {
				// Unpause
				GameManager.instance.Paused = false;

				// Do what we do on a fail
				OnWorldObjectSaveFail ();

				// Quit coroutine
				yield break;
			}

			// TODO: Update the spinner / loading graphic
			// use www.progress -- this  gives a value 0 to 1 on what percent is complete!

			// Draw next frame
			yield return null;
		}

		// Move the data returned into our JSON
		inputOutputJSON = www.text;

		// Unpause
		GameManager.instance.Paused = false;

		// If we got an error, process it.
		if (!ProcessError (www.text)) {
			yield break;
		}

		// Successful load
		OnWorldObjectSaveSuccess();
	}

	public IEnumerator DoFoodSave (InstanceFood obj) {
		Debug.Log ("Start Saving Food: "  + obj.UID + " | " + obj.sharedData.id );

		// Put the player's userID and token into the JSON object
		TempWorldObjectSaveData tempObject = new TempWorldObjectSaveData();
		tempObject.userid = GameManager.instance.playerData.userid;
		tempObject.token = GameManager.instance.playerData.token;

		// Set the variables
		tempObject.uid = obj.UID;
		tempObject.clientid = obj.sharedData.id;
		tempObject.lifespan = obj.sharedData.lifespan.value;
		tempObject.latitude = obj.location.latitude;
		tempObject.longitude = obj.location.longitude;
		// Note: Changing to ignore altitude -- always at 0
		tempObject.altitude = 0.0f;
		// tempObject.altitude = obj.location.altitude;
		tempObject.type = "food";

		// Set the JSON for saving
		inputOutputJSON = JsonUtility.ToJson (tempObject);

		// Save the time we started trying to load
		float startTime = Time.time;

		// Pause the game
		GameManager.instance.Paused = true;

		// Put the JSON into the only form element
		WWWForm form = new WWWForm();
		form.AddField("json", inputOutputJSON);

		// Start loading the webpage
		WWW www = new WWW (WorldObjectSaveWebURL, form);

		// TODO: Start showing the loading icon

		// While it is not finished saving
		while (!www.isDone) {
			// If we have timed out, quit our coroutine
			if (Time.time > startTime + timeOutTime) {
				// Unpause
				GameManager.instance.Paused = false;

				// Do what we do on a fail
				OnWorldObjectSaveFail ();

				// Quit coroutine
				yield break;
			}

			// TODO: Update the spinner / loading graphic
			// use www.progress -- this  gives a value 0 to 1 on what percent is complete!

			// Draw next frame
			yield return null;
		}

		// Move the data returned into our JSON
		inputOutputJSON = www.text;

		// Unpause
		GameManager.instance.Paused = false;

		// Successful load
		OnWorldObjectSaveSuccess();
	}

	public IEnumerator DoBeaconSave (InstanceBeacon obj) {
		Debug.Log ("Start Saving Beacon: "  + obj.UID + " | " + obj.sharedData.id );

		// Put the player's userID and token into the JSON object
		TempWorldObjectSaveData tempObject = new TempWorldObjectSaveData();
		tempObject.userid = GameManager.instance.playerData.userid;
		tempObject.token = GameManager.instance.playerData.token;

		// Set the variables
		tempObject.uid = obj.UID;
		tempObject.clientid = obj.sharedData.id;
		tempObject.lifespan = obj.sharedData.lifespan.value;
		tempObject.latitude = obj.location.latitude;
		tempObject.longitude = obj.location.longitude;
		// Note: Changing to ignore altitude -- always at 0
		tempObject.altitude = 0.0f;
		// tempObject.altitude = obj.location.altitude;
		tempObject.type = "beacon";

		// Set the JSON for saving
		inputOutputJSON = JsonUtility.ToJson (tempObject);

		// Save the time we started trying to load
		float startTime = Time.time;

		// Pause the game
		GameManager.instance.Paused = true;

		// Put the JSON into the only form element
		WWWForm form = new WWWForm();
		form.AddField("json", inputOutputJSON);

		// Start loading the webpage
		WWW www = new WWW (WorldObjectSaveWebURL, form);

		// TODO: Start showing the loading icon

		// While it is not finished saving
		while (!www.isDone) {
			// If we have timed out, quit our coroutine
			if (Time.time > startTime + timeOutTime) {
				// Unpause
				GameManager.instance.Paused = false;

				// Do what we do on a fail
				OnWorldObjectSaveFail ();

				// Quit coroutine
				yield break;
			}

			// TODO: Update the spinner / loading graphic
			// use www.progress -- this  gives a value 0 to 1 on what percent is complete!

			// Draw next frame
			yield return null;
		}

		// Move the data returned into our JSON
		inputOutputJSON = www.text;

		// Unpause
		GameManager.instance.Paused = false;

		// If we got an error, process it.
		if (!ProcessError (www.text)) {
			yield break;
		}

		// Successful load
		OnWorldObjectSaveSuccess();
	}


	/***************************************************
	 * Function: OnWorldObjectSaveFail
	 * Purpose: Event that runs when the player fails to save
	 * Inputs: None
	 * Returns: None
	 * *************************************************/
	private void OnWorldObjectSaveFail() {
		Debug.Log ("World Object Save fail");
	}

	/***************************************************
	 * Function: OnWorldObjectSaveSuccess
	 * Purpose: Event that runs when the player saves
	 * Inputs: None
	 * Returns: None
	 * *************************************************/
	private void OnWorldObjectSaveSuccess () {
		Debug.Log ("World Object Save success");

		// Everytime we save, reload the world, so that object is in the world
		DoWorldLoad (GameManager.instance.playerLocation);
	}

/*********************************************************************** LOAD WORLD ****************************************************************************/

	/***************************************************
	* Function: LoadWorldObjects
	* Purpose: Loads objects from the database
	* Inputs: 
	* Returns: 
	* *************************************************/
	public void LoadWorldData (GPSLocation coords) {

		// Get the info from the PHP page based on this data
		StartCoroutine("DoWorldLoad", coords);
	}


	/***************************************************
	 * DO THE LOADING OF THE PLAYER 
	 *************************************************/
	public IEnumerator DoWorldLoad ( GPSLocation GPSData ) {
		Debug.Log ("Start Loading World");

		// Put the player's data into the JSON object
		inputOutputJSON = "{";
		inputOutputJSON += "\"userid\": \""+GameManager.instance.playerData.userid+"\", ";
		inputOutputJSON += "\"token\": \""+GameManager.instance.playerData.token+"\", ";
		inputOutputJSON += "\"latitude\": \""+GPSData.latitude+"\", ";
		inputOutputJSON += "\"longitude\": \""+GPSData.longitude+"\", ";
		inputOutputJSON += "\"altitude\": \""+GPSData.altitude+"\" ";
		inputOutputJSON += "}";

		// Save the time we started trying to load
		float startTime = Time.time;

		// Pause the game
		GameManager.instance.Paused = true;

		// Put the JSON into the only form element
		WWWForm form = new WWWForm();
		form.AddField("json", inputOutputJSON);

		// Start loading the webpage
		WWW www = new WWW (WorldObjectLoadWebURL, form);

		// TODO: Start showing the loading icon

		// While it is not finished loading
		while (!www.isDone) {
			// If we have timed out, quit our coroutine
			if (Time.time > startTime + timeOutTime) {
				// Unpause
				GameManager.instance.Paused = false;

				// Do what we do on a fail
				OnWorldLoadFail ();

				// Quit coroutine
				yield break;
			}

			// TODO: Update the spinner / loading graphic
			// use www.progress -- this  gives a value 0 to 1 on what percent is complete!

			// Draw next frame
			yield return null;
		}

		// Move the data returned into our JSON
		inputOutputJSON = www.text;

		// Unpause
		GameManager.instance.Paused = false;

		// If we got an error, process it.
		if (!ProcessError (www.text)) {
			yield break;
		}

		// Save the world
		TempWorld tempWorld = JsonUtility.FromJson<TempWorld> (inputOutputJSON);

		// Clear the world
		GameManager.instance.world.foods.Clear ();
		GameManager.instance.world.creatures.Clear ();
		GameManager.instance.world.beacons.Clear ();

		// For each object, create a new world object and copy it into the world
		for (int i = 0; i < tempWorld.objects.Count; i++) {

			switch (tempWorld.objects [i].type) {
			case "creature":
					// TODO: Set all data for creature
				InstanceCreature newCreature = new InstanceCreature (Helpers.FindCreatureFromID (tempWorld.objects [i].clientid));
				newCreature.UID = tempWorld.objects [i].uid;
				newCreature.customName = tempWorld.objects [i].customName;
				newCreature.owner = tempWorld.objects [i].owner;
				newCreature.isFavorite = (tempWorld.objects [i].isFavorite == "true");
				newCreature.expiration = double.Parse(tempWorld.objects[i].expiration);

				newCreature.location = new GPSLocation ();
				newCreature.location.latitude = double.Parse(tempWorld.objects [i].latitude);
				newCreature.location.longitude = double.Parse(tempWorld.objects [i].longitude);
				// Note: Changing to ignore altitude -- always at 0
				newCreature.location.altitude = 0.0f;
				// newCreature.location.altitude = tempWorld.objects [i].altitude;
						
				GameManager.instance.world.creatures.Add (newCreature);
				Debug.Log ("Adding creature to world:" + newCreature.sharedData.id);
					break;

			case "beacon":
				InstanceBeacon newBeacon = new InstanceBeacon (Helpers.FindBeaconFromID (tempWorld.objects [i].clientid));
				newBeacon.UID = tempWorld.objects [i].uid;
				newBeacon.owner = tempWorld.objects [i].owner;
				newBeacon.expiration = double.Parse(tempWorld.objects[i].expiration);

				newBeacon.location = new GPSLocation ();
				newBeacon.location.latitude = double.Parse(tempWorld.objects [i].latitude);
				newBeacon.location.longitude = double.Parse(tempWorld.objects [i].longitude);

				// Note: Changing to ignore altitude -- always at 0
				newBeacon.location.altitude = 0.0f;
				//newBeacon.location.altitude = tempWorld.objects[i].altitude;

				Debug.Log ("Adding beacon to world:" + newBeacon.sharedData.id);
				GameManager.instance.world.beacons.Add (newBeacon);
				break;
			case "food":
				InstanceFood newFood = new InstanceFood (Helpers.FindFoodFromID (tempWorld.objects [i].clientid));
				newFood.UID = tempWorld.objects [i].uid;
				newFood.owner = tempWorld.objects [i].owner;
				newFood.expiration = double.Parse(tempWorld.objects[i].expiration);

				newFood.location = new GPSLocation();
				newFood.location.latitude = double.Parse(tempWorld.objects[i].latitude);
				newFood.location.longitude = double.Parse(tempWorld.objects[i].longitude);

				// Note: Changing to ignore altitude -- always at 0
				newFood.location.altitude = 0.0f;
				// newFood.location.altitude = tempWorld.objects[i].altitude;

				Debug.Log ("Adding food to world:" + newFood.sharedData.id);
				GameManager.instance.world.foods.Add (newFood);
				break;
			}
		}

		// Successful load
		OnWorldLoadSuccess();
	}


	/***************************************************
	 * Function: OnWorldLoadFail
	 * Purpose: Event that runs when the player fails to load
	 * Inputs: None
	 * Returns: None
	 * *************************************************/
	private void OnWorldLoadFail() {
		Debug.Log ("World Object Load fail");
	}

	/***************************************************
	 * Function: OnWorldLoadSuccess
	 * Purpose: Event that runs when the player loads
	 * Inputs: None
	 * Returns: None
	 * *************************************************/
	private void OnWorldLoadSuccess () {
		Debug.Log ("World Object Load success");
	}


/*********************************************************************** HELPER FUNCTIONS ****************************************************************************/


	public bool ProcessError (string text) {
		if (Helpers.FirstFour (text) == "9999") {
			GameManager.instance.Paused = false;
			GameManager.instance.LoadScene ("NoGPS");
			Debug.Log ("GENERAL ERROR: "+text);
			return false;
		}
		if (Helpers.FirstFour (text) == "0001") {
			GameManager.instance.Paused = false;
			GameManager.instance.LoadScene ("LogoScene");
			Debug.Log ("WRONG/BAD TOKEN: "+text);
			return false;
		}
		if (Helpers.FirstFour (text) == "0002") {
			GameManager.instance.Paused = false;
			GameManager.instance.LoadScene ("LoginScene");
			Debug.Log ("PLAYER DOES NOT EXIST: "+text);
			return false;
		}
		if (Helpers.FirstFour (text) == "0003") {
			GameManager.instance.Paused = false;
			GameManager.instance.LoadScene ("NoGPS");
			Debug.Log ("TO MANY REQUESTS: "+text);
			return false;
		}
		if (Helpers.FirstFour (text) == "0004") {
			GameManager.instance.Paused = false;
			GameManager.instance.LoadScene ("NoGPS");
			Debug.Log ("SAVE FAILED: "+text);
			return false;
		}
		if (Helpers.FirstFour (text) == "0005") {
			GameManager.instance.Paused = false;
			GameManager.instance.LoadScene ("NoGPS");
			Debug.Log ("NO DATA SENT: "+text);
			return false;
		}

		// No errors! Return true!
		return true;
	}

	/// <summary>
	/// Coverts a List of TempObjects to a List of InstanceCreatures.
	/// </summary>
	/// <returns>The objects to instance creatures.</returns>
	/// <param name="listOfTempObjects">List of temp objects.</param>
	private List<InstanceCreature> TempObjectsToInstanceCreatures ( List<TempObject> listOfTempObjects ) {
		List<InstanceCreature> temp = new List<InstanceCreature> ();
		for (int i = 0; i < listOfTempObjects.Count; i++) {
			temp.Add (TempObjectToInstanceCreature(listOfTempObjects [i]));
		}

		// Return the list
		return temp;
	}

	private List<InstanceFood> TempObjectsToInstanceFoods ( List<TempObject> listOfTempObjects ) {
		List<InstanceFood> temp = new List<InstanceFood> ();
		for (int i = 0; i < listOfTempObjects.Count; i++) {
			temp.Add (TempObjectToInstanceFood(listOfTempObjects [i]));
		}

		// Return the list
		return temp;
	}

	private List<InstanceBeacon> TempObjectsToInstanceBeacons ( List<TempObject> listOfTempObjects ) {
		List<InstanceBeacon> temp = new List<InstanceBeacon> ();
		for (int i = 0; i < listOfTempObjects.Count; i++) {
			temp.Add (TempObjectToInstanceBeacon(listOfTempObjects [i]));
		}

		// Return the list
		return temp;
	}

	/// <summary>
	/// Coverts a TempObject to an InstanceCreature.
	/// </summary>
	/// <returns>The object to instance creature.</returns>
	/// <param name="tempObject">Temp object.</param>
	private InstanceCreature TempObjectToInstanceCreature ( TempObject tempObject ) {

		ActorCreature prefab = null;
		for (int i=0; i<DataManager.instance.CreatureList.Length; i++) {
			if (tempObject.id == DataManager.instance.CreatureList [i].id) {
				prefab = DataManager.instance.CreatureList [i];
				break;
			}
		}
		InstanceCreature temp = new InstanceCreature(prefab);
		temp.UID = tempObject.uid;
		return temp;
	}

	private InstanceFood TempObjectToInstanceFood ( TempObject tempObject ) {

		ActorFood prefab = null;
		for (int i=0; i<DataManager.instance.FoodList.Length; i++) {
			if (tempObject.id == DataManager.instance.FoodList [i].id) {
				prefab = DataManager.instance.FoodList [i];
				break;
			}
		}
		InstanceFood temp = new InstanceFood(prefab);
		temp.UID = tempObject.uid;
		return temp;
	}

	private InstanceBeacon TempObjectToInstanceBeacon ( TempObject tempObject ) {

		ActorBeacon prefab = null;
		for (int i=0; i<DataManager.instance.BeaconList.Length; i++) {
			if (tempObject.id == DataManager.instance.BeaconList [i].id) {
				prefab = DataManager.instance.BeaconList [i];
				break;
			}
		}
		InstanceBeacon temp = new InstanceBeacon(prefab);
		temp.UID = tempObject.uid;
		return temp;
	}


	/// <summary>
	/// Creates the temp objects.
	/// </summary>
	/// <returns>The temp objects.</returns>
	/// <param name="inv">Inv.</param>
	private List<TempObject> CreateTempObjectsFromInventory(Inventory inv) {
		List<TempObject> tempObjects = new List<TempObject>();

		// Create temp objects for all the beacons
		for (int i = 0; i < inv.ItemCount; i++) {
			
			InstanceObject temp = inv.GetItem(i);

			// Create a save item
			TempObject tempItem = new TempObject();
			tempItem.id = temp.sharedData.id;
			//tempItem.displayName = temp.sharedData.displayName;
			tempItem.uid = temp.UID;

			// Add it to the list
			tempObjects.Add(tempItem);
		}

		return tempObjects;
	}

    /// <summary>
    /// This will fetch the bumbletum resource from the data manager.
    /// </summary>
    /// <param name="id"></param>
    /// <returns>A creature item for inventories. Null means it did not find it.</returns>
	public ActorCreature GetCreatureResourceById(string id)
    {
		ActorCreature item = null;

        for (int i = 0; i < CreatureList.Length; i++)
        {
            if(CreatureList[i].id == id)
            {
                item = CreatureList[i];
                break;
            }
        }

        return item;
    }




	// The data structures that we use for saving/loading/etc our player
	[System.Serializable]
	public struct TempWorld {
		public List<TempWorldObjectLoadData> objects;
	}


	[System.Serializable]
	public struct TempWorldObjectSaveData { 
		public string userid;
		public string token;
		public string uid;
		public string clientid;
		public double latitude;
		public double longitude;
		public double altitude;
		public float lifespan;
		public string type;
	}

	[System.Serializable]
	public struct TempWorldObjectLoadData {
		public string uid;
		public string owner;
		public string clientid;
		public string customName;
		public string isFavorite;
		public string latitude;
		public string longitude;
		public string altitude;
		public string expiration;
		public string type;
	}

	[System.Serializable]
	public struct TempPlayerLoginData { 
		public string userid;
		public string token;
	}

	[System.Serializable]
	public struct TempPlayerSaveData { 
		// Data stored in individual columns
		public string userid;
		public string token;
		public string displayName;
		public int coins;
		public int gems;
		// These are all stored in a "jsondata" column
		public List<TempObject> food;
		public List<TempObject> beacons;
		public List<TempObject> bumbletums;
		public List<GraveyardCreature> graveyard;
		public string avatarid;
		public string lastSpawnTimestamp;
	}

	[System.Serializable]
	public struct TempPlayerLoadData { 
		// Data stored in individual columns
		public string userid;
		public string token;
		public string displayName;
		public int coins;
		public int gems;
		// These are all stored in a "jsondata" column
		public List<TempObject> food;
		public List<TempObject> beacons;
		public List<TempObject> bumbletums;
		public List<GraveyardCreature> graveyard;
		public string avatarid;
		public string lastSpawnTimestamp;
	}

	[System.Serializable]
	public struct TempObject
	{
		public string uid;
		public string id;
		//public string displayName;
		//public int amount;
	}
}


