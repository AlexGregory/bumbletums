﻿/***********************************************************************************************
 * Class: GameManager 
 * Purpose: GameManager will run the core gameplay.
 * Notes: - Use GameManager.instance.____ to access any public methods or properties of this class.
 * Last Update: 9/7/16 - created GameManager and setup singleton - Hue
 * ********************************************************************************************/
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	//Singleton Instance
	public static GameManager instance;

	[Header("Game State")]
	public bool isPaused; // isPaused only stops player input, set "Paused = true" to stop EVERYTHING (Animations and input)
	public bool isPCDebug; // Are we debugging on a PC???

	[Header("Player and Object Info")]
	public PlayerData playerData;
	public Options options;
	public GPSLocation playerLocation; 

	[Header("World Information")]
	public World world;
	public InstanceCreature selectedCreature;

	[Header("Spawn Information")]
	public int maxCreaturesInWorld = 20;
	public ModifiedFloat creatureMaxSpawnDistance; // Distance from the player that creatures spawn
	public ModifiedFloat maxPersonalSpawns; // Most of our OWN spawns we are allowed to have in the world
	public ModifiedFloat minSpawnDelay; // The shortest time between spawns in seconds
	public ModifiedFloat minFoodSpawnTime; // The fastest a food will spawn
	public ModifiedFloat maxFoodSpawnTime; // The longest a food will spawn
	public ModifiedFloat foodMaxSpawnDistance; // The farthest a food will spawn from the player

	[Header("Display Options")]
	[Range(0.01f, 10.0f)]public float fadeSpeed = 1.5f; // Max Speed at which we fade to black in seconds
	public Image screenFader;

	// Holds the current inventory we are showing -- 
    [HideInInspector]
	public Inventory inventory;

	// Pause the game by setting to true or false
	public bool Paused {
		get { return isPaused; }
		set {
			isPaused = value;
			//Time.timeScale = isPaused ? 0 : 1;
		}
	}

	void Awake () {
		// Create Singleton 
		// Debug.Log("Singleton Created.");
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
	}

	void Start () {
		DataManager.instance.LoadResources ();
	}

	void OnValidate() {
		//Update the time scale
		Paused = isPaused;
	}


	public void LoadScene (string levelName) {
		Debug.Log ("Loading Scene: "+levelName);
		StartCoroutine ("DoLoadScene", levelName);
	}

	public IEnumerator DoLoadScene (string levelName) {
		Debug.Log ("Start Loading Scene: "+levelName);
		GameManager.instance.isPaused = true;

		yield return StartCoroutine("FadeToBlack");

		yield return SceneManager.LoadSceneAsync (levelName);

		yield return StartCoroutine("FadeToScene");

		GameManager.instance.isPaused = false;
		Debug.Log ("End Loading Scene: "+levelName);
	}

	public IEnumerator FadeToBlack() {
		//Debug.Log ("Start Fade Out");
		if (screenFader == null)
			yield break;

		while (screenFader.color.a < 1) {
			//Debug.Log ("Fade Out at: " + screenFader.color.a);
			screenFader.color = new Color (screenFader.color.r, screenFader.color.g, screenFader.color.b, Mathf.MoveTowards(screenFader.color.a, 1, (1 / fadeSpeed) * Time.deltaTime));
			yield return null;
		}

	}

	public IEnumerator FadeToScene() {
		if (screenFader == null)	
			yield break;

		//Debug.Log ("Start Fade In");
		while (screenFader.color.a > 0) {
			//Debug.Log ("Fade In at: " + screenFader.color.a);
			screenFader.color = new Color (screenFader.color.r, screenFader.color.g, screenFader.color.b, Mathf.MoveTowards(screenFader.color.a, 0, (1 / fadeSpeed) * Time.deltaTime));
			yield return null;
		}

		yield break;
	}


	IEnumerator LocationService()
	{
		// First, check if user has location service enabled
		if (!Input.location.isEnabledByUser) {
			Debug.Log("GPS Not Enabled");
			if (!GameManager.instance.isPCDebug) {
				// Go To NoGPS Scene
				GameManager.instance.LoadScene ("NoGPS");
			} 
			yield break;
		}

		// Start service before querying location
		Input.location.Start();

		//Wait until service initializes
		float maxWait = 20;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
		{
			yield return null;
			maxWait -= Time.deltaTime;
		}

		//Service didn't initialize in 20 seconds
		if (maxWait < 1)
		{
			Debug.Log("Timed out");
			if (!GameManager.instance.isPCDebug) {
				// Go To NoGPS Scene
				GameManager.instance.LoadScene ("NoGPS because service didn't in");
			}
			yield break;
		}

		// Connection has failed
		if (Input.location.status == LocationServiceStatus.Failed)
		{
			Debug.Log("Unable to determine device location");
			if (!GameManager.instance.isPCDebug) {
				// Go To NoGPS Scene
				GameManager.instance.LoadScene ("NoGPS because connection failed");
			}
			yield break;
		}

		if (!GameManager.instance.isPCDebug) {
			// Access granted and location value could be retrieved          
			GameManager.instance.playerLocation.latitude = Input.location.lastData.latitude;
			GameManager.instance.playerLocation.longitude = Input.location.lastData.longitude;             //Store the device's location in variables 

			// Note: Change to ignore altitude
			GameManager.instance.playerLocation.altitude = 0.0f;
			// GameManager.instance.playerLocation.altitude = Input.location.lastData.altitude;
		} 

			//centerLocation.latitude = localLatitude;
			//centerLocation.longitude = localLongitude;                     //Use the Device's location to center the google map on your location

		// Stop service if there is no need to query location updates continuously
		// Input.location.Stop();
	}
		
}
