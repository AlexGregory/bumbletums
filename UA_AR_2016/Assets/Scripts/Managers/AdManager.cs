﻿/*************************************************************************************************
 * Class - AdManager
 * Purpose - Singleton style class to manage advertisements. 
 * Notes: Use AdManager.instance.whatever to access methods and properties of this class
 * ***********************************************************************************************/

using UnityEngine;
using System.Collections;
#if UNITY_ADS
using UnityEngine.Advertisements; // only compile Ads code on supported platforms
#endif

public class AdManager : MonoBehaviour {
	public static AdManager instance = null;

	// Use this for initialization
	void Awake () {
		// Create Singleton 
		// Debug.Log("Singleton Created.");
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
	}

	// If we can show ads, this will start the ad, and start the coroutine to wait for it to be over
	public void ShowAd() {
		#if UNITY_ADS
		const string RewardedZoneId = "rewardedVideo";

		// If we aren't ready, return with an error
		if (!Advertisement.IsReady(RewardedZoneId)) {
			Debug.Log("Ad was not ready for zone "+RewardedZoneId);
			return;
		}

		// Set the options for our ad
		var options = new ShowOptions { resultCallback = OnAdView };
		Advertisement.Show(RewardedZoneId, options);

		// Pause and wait for the Ad to finish
		StartCoroutine(WaitForAd());

		#endif
	}

	public void OnAdView()
	{
		#if UNITY_ADS
		OnAdView (ShowResult.Finished);
		return;
		#endif
	}

	#if UNITY_ADS
	public void OnAdView (ShowResult result)
	{
		// When they finish watching an ad... Do what we do
		switch (result) {
			case ShowResult.Finished:
			Debug.Log ("Ad Complete!");
			break;
		case ShowResult.Skipped:
			Debug.Log ("ERROR: User skipped ad!");
			break;
		case ShowResult.Failed:
			Debug.Log ("ERROR: Ad Failed!");
			break;
		}
	}
	#endif

	// This is the coroutine that waits for the Ad to finish
	// Should only be called by accessing the ShowAd() function
	private IEnumerator WaitForAd()
	{
		// Set the isPaused variable in the GameManager - in case we need to pause any actions while the ad runs.
		GameManager.instance.Paused = true;

		// If we can show ads, do nothing else until the ad is finished.
		#if UNITY_ADS
		while (Advertisement.isShowing)
		yield return null;
		#endif 

		// Unpause
		GameManager.instance.Paused = false;

		// Stop the coroutine
		yield break;
	}


}
