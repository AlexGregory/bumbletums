﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class InventoryManager : MonoBehaviour {

	//Singleton Instance
	public static InventoryManager instance = null;

	[SerializeField]
	private GameObject inventoryView = null;
	[SerializeField]
	private GameObject itemView = null;

	[SerializeField]
	private GameObject closeupGameObject;
	public ActorObject currentActor = null;
	public InstanceObject currentItem = null;
	public ItemViewCamera itemViewCamera;
	public Transform CloseUpObjectPrototype;


	// Use this for initialization
	void Awake () {
		//Store the singleton
		if (instance == null)
			instance = this;
		else
			Destroy (this);

		// Turn off the prototype
		CloseUpObjectPrototype.gameObject.SetActive(false);
	}

	public void ShowInventory() {
		inventoryView.SetActive(true);
		itemView.SetActive(false);

		//Destory the previously viewed object
		Destroy (closeupGameObject);
	}


	public void ShowItem() {

		inventoryView.SetActive(false);
		itemView.SetActive(true);

		//Instantiate the item for viewing
		closeupGameObject = Instantiate (currentActor.gameObject, CloseUpObjectPrototype.position, CloseUpObjectPrototype.GetComponentInChildren<Transform>().rotation) as GameObject;
		//Remove the inventory item view component so the user cannot infinitely open the view
		Destroy(closeupGameObject.GetComponent<InventoryItemView>());
		Destroy(closeupGameObject.GetComponent<CreatureTapHandler>());
		Destroy(closeupGameObject.GetComponent<FoodTapHandler>());

		//Scale the item up for viewing
		closeupGameObject.transform.localScale = CloseUpObjectPrototype.localScale;
		// Update the text
		itemViewCamera.UpdateDisplay(currentActor);
	}
		
	public void ReturnToMap() {
		//GameManager.instance.inventory = null;
		GameManager.instance.LoadScene ("MainScene");
	}

	public void StartSelfieMode() {
		GameManager.instance.LoadScene ("FriendSelfies");
	}

	public void PlaceBeacon () {
		StartCoroutine (DoPlaceBeacon ());
	}

	public IEnumerator DoPlaceBeacon () {
		// Pause
		GameManager.instance.Paused = true;
		GameManager.instance.FadeToBlack ();

		// Set beacon
		InstanceBeacon beacon = (InstanceBeacon)currentItem;

		// Remove from inventory
		GameManager.instance.playerData.beaconInv.RemoveItem (currentItem);

		// Set its location
		beacon.location = GameManager.instance.playerLocation;

		// TODO: Remove this when it actually places in world on database
		GameManager.instance.world.beacons.Add(beacon);

		// Save 
		yield return DataManager.instance.StartCoroutine(DataManager.instance.DoBeaconSave(beacon));
		yield return DataManager.instance.StartCoroutine(DataManager.instance.DoPlayerSave ());

		// Load the world
		yield return DataManager.instance.DoWorldLoad (GameManager.instance.playerLocation);

		// Return to world
		ReturnToMap();
	}

}
