﻿using UnityEngine;
using System.Collections;

public class InventoryItemView : MonoBehaviour {

	#region Components

	public string inventoryGUID = null;

	#endregion

	#region MonoBehaviour Methods

	// Use this for initialization
	void Start () {
	}

	void OnMouseDown() {
		// Set this as the current object
		if (GetComponent<ActorBeacon> () != null) {
			InventoryManager.instance.currentItem = GameManager.instance.playerData.beaconInv.GetItem (GameManager.instance.playerData.beaconInv.GetIndexFromUID (inventoryGUID));
			InventoryManager.instance.currentActor = InventoryManager.instance.currentItem.sharedData;
		} else if (GetComponent<ActorCreature> () != null) {
			InventoryManager.instance.currentItem = GameManager.instance.playerData.creatureInv.GetItem (GameManager.instance.playerData.creatureInv.GetIndexFromUID (inventoryGUID));
			InventoryManager.instance.currentActor = InventoryManager.instance.currentItem.sharedData;
		} else if (GetComponent<ActorFood> () != null) {
			InventoryManager.instance.currentItem = GameManager.instance.playerData.foodInv.GetItem (GameManager.instance.playerData.foodInv.GetIndexFromUID (inventoryGUID));
			InventoryManager.instance.currentActor = InventoryManager.instance.currentItem.sharedData;
		} else {
			InventoryManager.instance.currentItem = null;
			InventoryManager.instance.currentActor = null; 
		}
			
		//Enable the Item View Camera / view
		InventoryManager.instance.ShowItem();
	}

	#endregion
}
