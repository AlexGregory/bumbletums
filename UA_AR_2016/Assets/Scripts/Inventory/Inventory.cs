﻿using UnityEngine;
using System;
using System.Collections.Generic;

[System.Serializable]
public class Inventory
{
	[SerializeField]
	private List<InstanceObject> objects = new List<InstanceObject>();
	public Action inventoryChanged;

    public int ItemCount
    {
        get { return objects.Count; }
    }

	public InstanceObject GetItem(int id)
	{
		InstanceObject ret = null;

		if (id < objects.Count && id >= 0)
			ret = objects [id];

		return ret;
    }

	public int GetIndexFromUID (string GUID) {
		for (int i = 0; i < objects.Count; i++) {			
			if (objects [i].UID == GUID) {
				return i;
			}
		} 
		// If not in list, return -1
		return -1;
	}

	public void AddItems<T>(List<T> items) where T : InstanceObject {
		for (int i = 0; i < items.Count; i++) {			
			AddItem(items[i]);
		}
	
		OnInventoryChanged ();
	}
		
	public void AddItems(List<InstanceObject> items) {
		for (int i = 0; i < items.Count; i++) {			
			AddItem(items[i]);
		}
	
		OnInventoryChanged ();
	}

	public void AddItem(InstanceObject item) {

		Debug.Log ("Adding item " + item.sharedData.displayName);
		// If there is a bonus item assigned
		if ( item.sharedData.rewardList.Count > 0 ) {
			// if this item would push us over our max list
			if (item.amount >= item.sharedData.maxAmount) {
				// Remove all items (put each object into testObj and check if it has the same id as item)
				objects.RemoveAll (testObj => testObj.sharedData.id == item.sharedData.id);
				// Give the bonus item!!!
				AwardRandomItemFromList ( item.sharedData.rewardList );
				// TODO: Spawn and destroy cool "BONUS ITEM" animation!
				Debug.Log ("TODO: Play Awarding Bonus Item Animation");
			}
		} else {
			// If no bonus item, but amount pushes us over the inventory -- show inventory full animation and quit
			if (item.amount >= item.sharedData.maxAmount) {
				// TODO: Spawn and destroy cool "Inventory Full" animation!
				Debug.Log ("TODO: Play InventoryFull Item Animation");
				// And quit without adding the item or changing the inventory
				return;
			}
		}

		// Then, add this item
		objects.Add (item);
		OnInventoryChanged ();
	}

	public void RemoveItem(InstanceObject item) {
		for (int i = 0; i < objects.Count; i++) {
			InstanceObject invItem = objects[i];
			//If they match
			if (invItem.UID == item.UID) {
				objects.RemoveAt (i);
			}
		}

		OnInventoryChanged ();
	}
				
	public void Clear() {
		objects.Clear ();
	}

	//Event Handler
	private void OnInventoryChanged(){
		if (inventoryChanged != null) {
			inventoryChanged ();
		}
	}

	private void AwardRandomItemFromList ( List<ActorObject> theList ) {

		ActorObject randomObject = theList [UnityEngine.Random.Range (0, theList.Count)];

		// Based on its type add it to inventory
		ActorBeacon asBeacon = randomObject.GetComponent<ActorBeacon>(); 
		ActorCreature asCreature = randomObject.GetComponent<ActorCreature>();
		ActorFood asFood = randomObject.GetComponent<ActorFood>();

		if (asCreature != null) {
			GameManager.instance.playerData.creatureInv.AddItem (new InstanceCreature(asCreature));
		} else if (asBeacon != null) {
			GameManager.instance.playerData.beaconInv.AddItem (new InstanceBeacon(asBeacon));
		} else if (asFood != null) {
			GameManager.instance.playerData.foodInv.AddItem (new InstanceFood(asFood));
		}

	}


}

