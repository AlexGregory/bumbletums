﻿using UnityEngine;
using System.Collections;

public class InventoryGrid : MonoBehaviour
{
    //Reference to the inventory to display on the screen
    private int minDisplay = 0;
    private int maxDisplay = 9;
    private Inventory inventory = null;
	private GameObject[,] objects = null;

    //Components
    private Transform tf = null;

    [SerializeField]
    private float spacing;
    [SerializeField]
    private int horizontalCount = 3;
    [SerializeField]
    private int verticalCount = 3;

	#region Properties

	public Inventory Inventory {
		get { return inventory; }
		set 
		{ 
			inventory = value; 
			//Update the display for the new inventory
			//UpdateGrid ();
		}
	}

	#endregion

    // Use this for initialization
    void Start()
    {
		//Get Components
		tf = GetComponent<Transform>();

		inventory = GameManager.instance.inventory;

        //When the inventory gains or loses an item, the grid will update
        //instead of every frame
        inventory.inventoryChanged += UpdateGrid;

        //Calculate how much can be shown on the screen
        maxDisplay = horizontalCount * verticalCount;
        //Create the array for the GameObjects that will be displayed
		objects = new GameObject[verticalCount, horizontalCount];

		UpdateGrid ();
    }


	void OnDestroy() {
		inventory.inventoryChanged -= UpdateGrid;
		ClearDisplay ();
	}

	// Update is called once per frame
	void Update()
	{
        //Temp Code: Use this to manually update the grid view
        //when you add items to the inventory, or change its horizontal and
        //vertical item counts
//		if (Input.GetKeyDown (KeyCode.I)) {
//            ClearDisplay();
//
//            //Calculate how much can be shown on the screen
//            maxDisplay = horizontalCount * verticalCount;
//            //Create the array for the GameObjects that will be displayed
//			objects = new GameObject[verticalCount, horizontalCount];
//
//            UpdateGrid ();
//		}
	}

	public void ScrollDown(){
		if (maxDisplay < inventory.ItemCount - 1) {
			minDisplay+=horizontalCount;
			maxDisplay+=horizontalCount;
		}

		UpdateGrid ();
	}

	public void ScrollUp(){
		if (minDisplay > 0) {
			minDisplay-=horizontalCount;
			maxDisplay-=horizontalCount;
		}

		UpdateGrid ();
	}

	void ClearDisplay()
	{
        for (int i = 0; i < objects.GetLength(0); i++)
        {
            for (int j = 0; j < objects.GetLength(1); j++)
            {
                //if the object already exists
                if (objects[i, j] != null)
                {
                    //destroy it
					Destroy(objects[i, j]);
                }
            }
        }
    }


	bool IsInGrid (string id) {
		for (int i = 0; i < verticalCount; i++) {
			for (int j = 0; j < horizontalCount; j++) {
				if (objects [i, j] != null) {
					ActorObject obj = objects [i, j].GetComponent<ActorObject> ();
					if (obj.id == id) {
						return true;
					}
				}
			}
		}

		return false;
	}

    void UpdateGrid()
	{
		ClearDisplay ();

		//get the size of the inventory
		int size = inventory.ItemCount;
        
		// Start at 0,0
		int i = 0;
		int j = 0;

		// Store how many we have drawn
		int numItemsOnScreen = 0;

		// Count from minDisplay to minDisplay + numberOfItemsOnScreen
		int numItemsOnScreenMax = horizontalCount * verticalCount;

		// Start at our minDisplay (whats our first item);  as long as we haven't filled the screen; draw the next one...
		for (int index = minDisplay; numItemsOnScreen < numItemsOnScreenMax; index++ )
        {
			// If we haven't run out of objects
			if (index<size)
            {
                //if the object already exists
                if (objects[i, j] != null)
                {
                    //destroy it
					Destroy(objects[i, j]);
                }

                //grab a reference of the inventory item
				GameObject prefabToInstantiate = inventory.GetItem(index).sharedData.gameObject;


                //If the prefab exists
				if (prefabToInstantiate != null) {
					// If we have NOT already instantiated one...
					if (!IsInGrid (inventory.GetItem (index).sharedData.id)) {

						//Calculate the inventory position
						Vector3 invPosition = tf.position + (new Vector3 (-j, -i, 0) * spacing);

						//instantiate the prefab and store the gameobject
						objects [i, j] = Instantiate (prefabToInstantiate, invPosition, Quaternion.identity, tf) as GameObject;

						// Remove the tap handlers
						Destroy (objects [i, j].GetComponent<CreatureTapHandler> ());
						Destroy (objects [i, j].GetComponent<FoodTapHandler> ());

						// Name it for debug
						objects [i,j].name = "Item:"+index+"/"+size+" ("+i+","+j+")";
							

						// Add the click handler for inventory objects and set the GUID so we can reference back to the inventory object!
						InventoryItemView iiv = objects [i, j].AddComponent (typeof(InventoryItemView)) as InventoryItemView;
						iiv.inventoryGUID = inventory.GetItem (index).UID;

						// Increase the count of how many we have drawn
						numItemsOnScreen++;

						// Move down 1
						i++;

						// If we have gone passed our number of rows, then move to the next column
						if (i > horizontalCount) {
							i = 0; 
							j++;
						}
					}
				} else {
					// We have displayed everything, so break the loop!
					break;
				}
            }
        }
    }

   

}
