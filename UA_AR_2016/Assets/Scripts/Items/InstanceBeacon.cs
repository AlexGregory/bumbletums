﻿using UnityEngine;
using System;
using System.Collections;

[System.Serializable]
public class InstanceBeacon : InstanceObject  {

	[Header("Instance Data")]
	public string owner; // User id of the person who placed this object
	public GPSLocation location;
	public double expiration; 

	public override int amount { 
		get {
			int tempCount = 0;
			for (int i = 0; i < GameManager.instance.playerData.beaconInv.ItemCount; i++) {
				if (GameManager.instance.playerData.beaconInv.GetItem (i).sharedData.id == sharedData.id) {
					tempCount++;
				}
			}
			return tempCount;
		}
	}

	public InstanceBeacon ( ActorBeacon source ) {
		// Set a new UID
		UID = System.Guid.NewGuid ().ToString();
		sharedData = source;
	}

	public InstanceBeacon ( GameObject prefab ) {
		UID = System.Guid.NewGuid ().ToString();	
		sharedData = prefab.GetComponent<ActorBeacon> ();
	}


}
