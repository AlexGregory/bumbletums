﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActorBeacon : ActorObject {

	[Tooltip("Distance of Beacon in GPS Coordinates")]public ModifiedFloat radius; // How far beacon affects players
	public List<Status> avatarEffects;

	//[HideInInspector]
	[Header("Ignore me!")]
	public InstanceBeacon instanceData;

	// Use this for initialization
	void Start () {
	
		// If we don't link to our prefab, call our constructor
		if (instanceData == null)
			instanceData = new InstanceBeacon (this);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
