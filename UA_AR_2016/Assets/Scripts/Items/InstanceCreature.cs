﻿using UnityEngine;
using System;
using System.Collections;

[System.Serializable]
public class InstanceCreature : InstanceObject {

	[Header("Instance Data")]
	public string owner; // User id of the person who placed this object
	public GPSLocation location;
	public double expiration; 

	[Header("Personalization Variables")]
	public string customName; //name of the creature - if this is not null, use this instead of display name
	public bool isFavorite; //whether or not the creature is listed as a favorite

	public override int amount { 
		get {
			int tempCount = 0;
			for (int i = 0; i < GameManager.instance.playerData.creatureInv.ItemCount; i++) {
				if (GameManager.instance.playerData.creatureInv.GetItem (i).sharedData.id == sharedData.id) {
					tempCount++;
				}
			}
			return tempCount;
		}
	}

	public InstanceCreature ( ActorCreature source ) {
		// Set a new UID
		UID = System.Guid.NewGuid ().ToString();
		sharedData = source;

		//GameManager.instance.world.creatures.Add(this);
	}

	public InstanceCreature ( GameObject prefab ) {
		UID = System.Guid.NewGuid ().ToString();	
		sharedData = prefab.GetComponent<ActorCreature> ();

		//GameManager.instance.world.creatures.Add(this);
	}


}
