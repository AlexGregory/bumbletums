﻿// This is the "Pure Data" version of a food -- it only contains the info required to make a food, plus a link to the prefab/shared settings
using UnityEngine;
using System;
using System.Collections;

[System.Serializable]
public class InstanceFood : InstanceObject {

	[Header("Instance Data")]
	public string owner; // User id of the person who placed this object
	public GPSLocation location;
	public double expiration; 
	// May not need -- just use typeof() -- public WorldObjectType type; // What type of object is it?

	public override int amount { 
		get {
			int tempCount = 0;
			for (int i = 0; i < GameManager.instance.playerData.foodInv.ItemCount; i++) {
				if (GameManager.instance.playerData.foodInv.GetItem (i).sharedData.id == sharedData.id) {
					tempCount++;
				}
			}
			return tempCount;
		}
	}

	public InstanceFood ( ActorFood source ) {
		// Set a new UID
		UID = System.Guid.NewGuid ().ToString();
		sharedData = source;
	}

	public InstanceFood ( GameObject prefab ) {
		UID = System.Guid.NewGuid ().ToString();	
		sharedData = prefab.GetComponent<ActorFood> ();
	}
		
}
