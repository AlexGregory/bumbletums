﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class InstanceObject {

	[Header("UID")]
	public string UID; // Unique Identifier

	[Header("Prefab")]
	public ActorObject sharedData;

	public virtual int amount { get; set; }

	public GameObject CreateActor (Vector3 pos, Quaternion rot, Transform parent=null) {
		if (sharedData == null) {
			Debug.LogError ("ERROR: Attempted to create gameobject from Instance Object, but object has no shared data.");
			return null;
		}

		GameObject temp = GameObject.Instantiate(sharedData.gameObject, pos, rot, parent) as GameObject;

		if (temp.GetComponent<ActorCreature> () != null) {
			temp.GetComponent<ActorCreature> ().instanceData = (InstanceCreature)this;
			return temp;
		}
		if (temp.GetComponent<ActorBeacon> () != null) {
			temp.GetComponent<ActorBeacon> ().instanceData = (InstanceBeacon)this;
			return temp;
		}
		if (temp.GetComponent<ActorFood> () != null) {
			temp.GetComponent<ActorFood> ().instanceData = (InstanceFood)this;
			return temp;
		}

		Debug.LogError ("ERROR: TRIED TO CREATE AN ACTOR WITHOUT AN ACTORXXXX COMPONENT.");
		return temp;

	}

}
