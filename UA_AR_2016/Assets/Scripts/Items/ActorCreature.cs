﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class ActorCreature : ActorObject {

	// Amount literally counts how many are in our inventory
	[Tooltip("Percent: 0 to 1")]public ModifiedFloat catchChance;//base percentage chance to capture a creature
	[Tooltip("Percent: 0 to 1")]public ModifiedFloat fleeChance;//base percentage for the creature to flee
	[Tooltip("Odds: 0 never spawns - 100 is normal - 1000 spawns all the damn time.")]public ModifiedFloat spawnChance; //actual chance for the creature to spawn
	[Tooltip("Percent: 0 to 1")]public ModifiedFloat visibilityChance; // chance for the creature to be visible on the map (assuming it has spawned)
	// TODO: public IItem rewardForMax; // Reward we give when we hit max number of these
	public List<Status> avatarEffects;


	//[HideInInspector]
	[Header("Ignore me!")]
	public InstanceCreature instanceData;

	// Use this for initialization
	void Start () {
		// If we don't link to our prefab, or have a unique id
		// call our constructor to generate the instance
		if (instanceData == null || instanceData.UID == "") {
			ActorCreature prefab = Helpers.FindCreatureFromID (this.id);
			instanceData = new InstanceCreature (prefab);
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
