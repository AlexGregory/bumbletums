﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActorObject : MonoBehaviour {

	[Header("Default Values")]
	public string id; // Used by Code to find an item
	public string displayName; // What we display to the player if the game shows a name
	[TextArea(3,5)] public string description; // What we display to the player if the game asks for a description
	public int maxAmount; // What is the most we should be allowed to hold
	[Tooltip("Number of seconds from spawn to disappear from world")]public ModifiedFloat lifespan; // How long the BT stays in the DB after it has spawned
	[Tooltip("A random item from this list will be given as a reward.\n If no objects in list (count=0), then 'inventory full' message will be given at maxAmount.")]public List<ActorObject> rewardList; 

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
