﻿using UnityEngine;
using System.Collections;

public class ActorFood : ActorObject {

	[Tooltip("Percent: -1 to 1")]public ModifiedFloat catchChance; //base percentage increase to the chance to capture a creature
	[Tooltip("Percent: -1 to 1")]public ModifiedFloat fleeChance; //base percentage increase to the chance for a creature to flee
	[Tooltip("Odds: 0 never spawns - 100 is normal - 1000 spawns all the damn time.")]public ModifiedFloat spawnChance; //actual chance for the creature to spawn
	[Tooltip("Percent: 0 to 1")]public ModifiedFloat visibilityChance; // chance for the creature to be visible on the map (assuming it has spawned)

	//[HideInInspector]
	[Header("Ignore me!")]
	public InstanceFood instanceData;

	// Use this for initialization
	void Start () {
	
		// If we don't link to our prefab, call our constructor
		if (instanceData == null)
			instanceData = new InstanceFood (this);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
