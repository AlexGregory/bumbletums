﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowPercent : MonoBehaviour {

	private Text percentage;
	private float percentChance;

	// Use this for initialization
	void Start () {
		percentage = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		//keeps chance between 0-100
		percentChance = Mathf.Clamp(FriendshipBar.fillProgress * 100f, 0f, 100f);
		//converts chance to text, formats to no decimal
		percentage.text = percentChance.ToString("#");
	}
}
