﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FriendshipBar : MonoBehaviour {

	private Image progressBar;
	public static float fillProgress = 0.1f;

	private float fishFill = .3f;
	private float lemonFill = .2f;

	// Use this for initialization
	void Start () {
		progressBar = GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update () {	
		//clamps progress between 0-1
		fillProgress = Mathf.Clamp01 (fillProgress);
		//fills in bar with amount of progress
		progressBar.fillAmount = fillProgress;
	}

	public void FeedFish(){
		FriendshipBar.fillProgress += fishFill;
	}

	public void FeedLemon(){
		FriendshipBar.fillProgress -= lemonFill;
	}
}
