﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpinToCatch : MonoBehaviour {

	public Image spinAnim;
	private Vector3 rotSpin;
	private int spinTime;
	private int reset = 2000;

	// Use this for initialization
	void Start () {
		spinAnim = spinAnim.GetComponent<Image> ();
		rotSpin = spinAnim.transform.eulerAngles;
		rotSpin.z = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (rotSpin.z > spinTime) {
			rotate ();
		}
	
	}

	public void SpinBar()
	{
		spinTime = Random.Range (1000, 1500);
		rotSpin.z = reset;
	}

	void rotate(){
		for (int i = 0; i < spinTime; i++) {
			rotSpin.z -= Time.deltaTime;
			spinAnim.transform.eulerAngles = rotSpin;
		}
	}
}
