﻿using UnityEngine;
using System.Collections;

public class SoundVolume : MonoBehaviour {

	private AudioSource audioSource = null;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (audioSource != null && GameManager.instance != null) {
			audioSource.volume = GameManager.instance.options.VolumeMusic;
		}
	}
}
