﻿using UnityEngine;
using System.Collections;

public class NoGPSScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameManager.instance.Paused = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnAdButton() {
		AdManager.instance.ShowAd ();
	}

	public void OnLoginButton () {
		GameManager.instance.LoadScene ("Start");
	}

}
