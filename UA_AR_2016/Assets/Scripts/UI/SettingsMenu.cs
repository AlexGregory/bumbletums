﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SettingsMenu : MonoBehaviour {

	public Toggle isAudioOnToggle;
	public GameObject Sounds;
	// Use this for initialization
	void Start () {
		Sounds = GameObject.FindGameObjectWithTag ("Sounds");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable () {
//		if (GameManager.instance.options.VolumeMusic > 0.0f) {
//			isAudioOnToggle.isOn = true;
//		} else {
//			isAudioOnToggle.isOn = false;
//		}
	}

	public void OnSoundChange () {
		if (isAudioOnToggle.isOn) {
			GameManager.instance.options.VolumeMusic = 1.0f;

		} else {
			GameManager.instance.options.VolumeMusic = 0.0f;
		}
	}
}
