﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;



public class PlayerMenu : MonoBehaviour {

	public InputField PlayerNameTextbox;
	public Text EmailTextbox;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnNameChange () {
		GameManager.instance.playerData.displayName = PlayerNameTextbox.text;
		DataManager.instance.SavePlayerData ();
	}


	void OnEnable () {
		PlayerNameTextbox.text = GameManager.instance.playerData.displayName;
		EmailTextbox.text = "("+GameManager.instance.playerData.userid+")";

	}

	void OnDisable () {
	}
}
