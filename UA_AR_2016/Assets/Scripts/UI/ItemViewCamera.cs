﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemViewCamera : MonoBehaviour {

	public Text ItemNameTextbox;
	public Text ItemDescriptionTextbox;
	public Text ItemCountTextbox;

	public Button selfieButton;
	public Button makeAvatarButton;
	public Button placeBeaconButton;
	public Image isAvatarImage;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable () {
		if (InventoryManager.instance.currentActor != null) {
			UpdateDisplay (InventoryManager.instance.currentActor);
		}
	}

	public void UpdateDisplay (ActorObject item) 
	{
		ActorCreature itemCreature = item.GetComponent<ActorCreature>();
		ActorBeacon itemBeacon = item.GetComponent<ActorBeacon>(); // In case there is extra beacon data in the future -- if itemBeacon != null, we have a beacon and can grab it's data
		ActorFood itemFood = item.GetComponent<ActorFood>(); // Same for foods!

		//FUTURE_FEATURE: If it is a creature with custom name, then return custom name
		//if (itemCreature != null && itemCreature.instanceData.customName != null) {
		//	ItemNameTextbox.text = itemCreature.instanceData.customName;
		//} else {
		//	ItemNameTextbox.text = item.displayName;
		//}
		ItemNameTextbox.text = item.displayName;
		ItemDescriptionTextbox.text = item.description;

		//QUICK FIX
		ItemCountTextbox.text = InventoryManager.instance.currentItem.amount  + " / " + item.maxAmount;

		// If it is NOT a creature
		if (itemCreature == null) {
			// Turn off selfie button
			selfieButton.gameObject.SetActive (false);
			// Turn off all avatar buttons/images
			makeAvatarButton.gameObject.SetActive(false);
			isAvatarImage.gameObject.SetActive (false);

			// If it is not a beacon
			if (itemBeacon == null) {
				placeBeaconButton.gameObject.SetActive (false);
			} else {
				// It is a beacon
				placeBeaconButton.gameObject.SetActive (true);
			}

		} else {
			// Turn on selfie button
			selfieButton.gameObject.SetActive (true);

			// Turn off place beacon button
			placeBeaconButton.gameObject.SetActive (false);

			// If it is our favorite
			if (GameManager.instance.playerData.currentAvatar == item) {
				// Turn ON is favorite, turn off button
				makeAvatarButton.gameObject.SetActive(false);
				isAvatarImage.gameObject.SetActive (true);
			} else {
				// Turn OFF is favorite, turn on make avatar button
				makeAvatarButton.gameObject.SetActive(true);
				isAvatarImage.gameObject.SetActive (false);
			}
		}

	}

	public void SetAsAvatar() {
		GameManager.instance.playerData.currentAvatar = InventoryManager.instance.currentActor as ActorCreature;
		makeAvatarButton.gameObject.SetActive(false);
		isAvatarImage.gameObject.SetActive (true);

		DataManager.instance.SavePlayerData ();
	}

	public void placeBeacon () {
		InventoryManager.instance.PlaceBeacon ();
	}
}
