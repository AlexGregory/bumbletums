﻿using UnityEngine;
using System.Collections;

public class ScrollUpAndLoop : MonoBehaviour {

	public float scrollSpeed = 10.0f;
	public Vector3 startPosition;

	private Transform tf;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		startPosition = tf.position;
	}
	
	// Update is called once per frame
	void Update () {

		if (tf.position.y > Screen.height) {
			tf.position = startPosition;
		} else {
			tf.position = new Vector3 (tf.position.x, tf.position.y + (scrollSpeed * Time.deltaTime), tf.position.z);
		}
	}
}
