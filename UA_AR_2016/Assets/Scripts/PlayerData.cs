﻿/***********************************************************************************************
 * Structure: PlayerData 
 * Purpose: Holds all data about the player
 * Notes: Referenced through the GameManager - generally, you will want to use GameManager.instance.player to access this.
 * ********************************************************************************************/
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PlayerData {

	[Header("Info")]
	public string userid; // Usually their email address
	public string displayName; // They can change this as needed.
	public ActorCreature currentAvatar; // The current avatar they are using
	public Graveyard graveyard = new Graveyard(); // This is a list of strings of BTs that are not visible because they ran away or are otherwise disposed of.
	public double lastSpawnTimestamp; // this is the last time we spawned a creature


	[Header("Status Effects")]
	public List<Status> readOnlyStatusEffects;
	public List<Status> statusEffects {
		get {
			List<Status> temp = AssembleStatusList ();
			readOnlyStatusEffects = temp;
			return temp;
		}
	}

	[Header("Inventory")]
	public int coins;
	public int gems;
    public Inventory foodInv = new Inventory(); 
    public Inventory beaconInv = new Inventory();
    public Inventory creatureInv = new Inventory();

	[TextArea(3,10)]
	public string token;


	private List<Status> AssembleStatusList ( ) {

		List<Status> finalList = new List<Status> ();

		// Add status effects from avatar
		if (GameManager.instance.playerData.currentAvatar != null) {
			for (int i = 0; i < GameManager.instance.playerData.currentAvatar.avatarEffects.Count; i++) {
				// If it isn't in the list, add it
				if (!IsStatusInList (GameManager.instance.playerData.currentAvatar.avatarEffects [i], finalList)) {
					finalList.Add (GameManager.instance.playerData.currentAvatar.avatarEffects [i]);
				}
			}
		}

		// Add status effects from beacons
		for (int i = 0; i < GameManager.instance.world.beacons.Count; i++) {
			InstanceBeacon currentBeacon = GameManager.instance.world.beacons [i];
			Vector3 beaconLocation = new Vector3 ((float)currentBeacon.location.latitude, (float)currentBeacon.location.longitude, 0.0f );			
			Vector3 playerLocation = new Vector3 ((float)GameManager.instance.playerLocation.latitude, (float)GameManager.instance.playerLocation.longitude, 0.0f );			
			ActorBeacon beaconActor = currentBeacon.sharedData as ActorBeacon;
			if (Vector3.Distance (beaconLocation, playerLocation) < beaconActor.radius.value) {
				for (int statusIndex = 0; statusIndex < beaconActor.avatarEffects.Count; statusIndex++) {
					// If it isn't in the list, add it
					if (!IsStatusInList (beaconActor.avatarEffects [statusIndex], finalList)) {
						finalList.Add (beaconActor.avatarEffects [statusIndex]);
					}
				}
			} else {
				Debug.Log("Beacon ("+beaconActor.id+"|"+beaconActor.displayName+") out of range.");
			}
		}

		// Add status effects from the world/weather
		for (int i = 0; i < GameManager.instance.world.statuses.Count; i++) {
			// If it isn't in the list, add it
			if (!IsStatusInList (GameManager.instance.world.statuses[i], finalList)) {
				finalList.Add (GameManager.instance.world.statuses[i]);
			}
		}

		// Add date/time status effects
		Status monthStatus = new Status();
		monthStatus.displayIcon = null;
		switch (DateTime.Now.Month) {
		case 1:
			monthStatus.id = "TIME_JAN";
			monthStatus.displayName = "January Joy!";
			// If it isn't in the list, add it
			if (!IsStatusInList (monthStatus, finalList)) {
				finalList.Add (monthStatus);
			}
			break;
		case 2:
			monthStatus.id = "TIME_FEB";
			monthStatus.displayName = "February Fun!";
			// If it isn't in the list, add it
			if (!IsStatusInList (monthStatus, finalList)) {
				finalList.Add (monthStatus);
			}
			break;
		case 3:
			monthStatus.id = "TIME_MAR";
			monthStatus.displayName = "March Madness!";
			// If it isn't in the list, add it
			if (!IsStatusInList (monthStatus, finalList)) {
				finalList.Add (monthStatus);
			}
			break;
		case 4:
			monthStatus.id = "TIME_APR";
			monthStatus.displayName = "April Awesomeness!";
			// If it isn't in the list, add it
			if (!IsStatusInList (monthStatus, finalList)) {
				finalList.Add (monthStatus);
			}
			break;
		case 5:
			monthStatus.id = "TIME_MAY";
			monthStatus.displayName = "May Merriment!";
			// If it isn't in the list, add it
			if (!IsStatusInList (monthStatus, finalList)) {
				finalList.Add (monthStatus);
			}
			break;
		case 6:
			monthStatus.id = "TIME_JUN";
			monthStatus.displayName = "June Joyfulness!";
			// If it isn't in the list, add it
			if (!IsStatusInList (monthStatus, finalList)) {
				finalList.Add (monthStatus);
			}
			break;
		case 7:
			monthStatus.id = "TIME_JUL";
			monthStatus.displayName = "July Jubilation";
			// If it isn't in the list, add it
			if (!IsStatusInList (monthStatus, finalList)) {
				finalList.Add (monthStatus);
			}
			break;
		case 8:
			monthStatus.id = "TIME_AUG";
			monthStatus.displayName = "August Elation!";
			// If it isn't in the list, add it
			if (!IsStatusInList (monthStatus, finalList)) {
				finalList.Add (monthStatus);
			}
			break;
		case 9:
			monthStatus.id = "TIME_SEP";
			monthStatus.displayName = "September Smiles!";
			// If it isn't in the list, add it
			if (!IsStatusInList (monthStatus, finalList)) {
				finalList.Add (monthStatus);
			}
			break;
		case 10:
			monthStatus.id = "TIME_OCT";
			monthStatus.displayName = "October Euphoria!";
			// If it isn't in the list, add it
			if (!IsStatusInList (monthStatus, finalList)) {
				finalList.Add (monthStatus);
			}
			break;
		case 11:
			monthStatus.id = "TIME_NOV";
			monthStatus.displayName = "November Wonder";
			// If it isn't in the list, add it
			if (!IsStatusInList (monthStatus, finalList)) {
				finalList.Add (monthStatus);
			}
			break;
		case 12:
			monthStatus.id = "TIME_DEC";
			monthStatus.displayName = "December Blowout!";
			// If it isn't in the list, add it
			if (!IsStatusInList (monthStatus, finalList)) {
				finalList.Add (monthStatus);
			}
			break;
		default:
			break;
		}

		Status timeStatus = new Status();
		timeStatus.displayIcon = null;
		if (DateTime.Now.Hour > 0 && DateTime.Now.Hour <= 6) {
			timeStatus.id = "TIME_GRAVEYARD";
			timeStatus.displayName = "Graveyard Shift!";
			// If it isn't in the list, add it
			if (!IsStatusInList (timeStatus, finalList)) {
				finalList.Add (timeStatus);
			}
		} else if (DateTime.Now.Hour > 6 && DateTime.Now.Hour <= 12) {
			timeStatus.id = "TIME_MORNING";
			timeStatus.displayName = "Rise and Shine!";
			// If it isn't in the list, add it
			if (!IsStatusInList (timeStatus, finalList)) {
				finalList.Add (timeStatus);
			}
		} else if (DateTime.Now.Hour > 12 && DateTime.Now.Hour <= 18) {
			timeStatus.id = "TIME_AFTERNOON";
			timeStatus.displayName = "Afternoon Delight!";
			// If it isn't in the list, add it
			if (!IsStatusInList (timeStatus, finalList)) {
				finalList.Add (timeStatus);
			}
		} else if (DateTime.Now.Hour > 18 && DateTime.Now.Hour <= 24) {
			timeStatus.id = "TIME_NIGHT";
			timeStatus.displayName = "Night Owl!";
			// If it isn't in the list, add it
			if (!IsStatusInList (timeStatus, finalList)) {
				finalList.Add (timeStatus);
			}
		} 	


		// Return the list
		return finalList;
	}

	public bool IsStatusInList (Status status, List<Status> list) {
		for (int i = 0; i < list.Count; i++) {
			if (list [i].id == status.id) {
				return true;
			}
		}
		return false;
	}
}

