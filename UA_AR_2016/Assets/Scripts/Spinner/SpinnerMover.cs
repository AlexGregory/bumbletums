﻿using UnityEngine;
using System.Collections;

public class SpinnerMover : MonoBehaviour {

	[Header("Passed in from Spinning Code")]
	public float displayVelocity;
	public float displayTargetRotation;

	[Header("Options")]
	public float minVelocityToSpin;
	public int extraRotationsPerVelocity = 4;
	public float extraSecondsPerVelocity = 2;
	[Tooltip("Needs to stay within 0 to 1 range")] public AnimationCurve speedCurve;
	public Transform spinnerPanelTransform;

	[Header("Private")]
	private bool isSpinning = false;
	private float _targetRotation;
	private float _startRotation;
	private float _currentRotation;
	private float _spinTime;
	private bool _spinClockwise;
	private float _velocity;
	private ArMode arMode;

	// Use this for initialization
	void Awake () {
		arMode = FindObjectOfType<ArMode> ();
	}

	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Spin the Ring of Friendship. Calls OnSpinComplete() when finished.
	/// </summary>
	/// <param name="velocity">Velocity - speed to spin circle. Should be between 0 and 1</param>
	/// <param name="targetRotation">Target rotation in degrees - use Percent01ToDegrees() if needed.</param>
	/// <param name="startRotation">Start rotation in degrees.</param>
	/// <param name="spinClockwise">If set to <c>true</c> spin clockwise.</param>
	public void Spin (float velocity, float targetRotation, float startRotation, bool spinClockwise) {
		displayVelocity = velocity;
		displayTargetRotation = targetRotation;

		// if Velocity is too low, then don't spin. 
		if (velocity < minVelocityToSpin) {
			return;
		}

		// Don't run if already spinning
		if (isSpinning)
			return;

		// Otherwise, set that we are spinning
		isSpinning = true;

		// Figure out our values
		_startRotation = startRotation;
		_currentRotation = startRotation;
		_targetRotation = targetRotation;
		_spinClockwise = spinClockwise;
		_velocity = velocity;

		// Also use velocity to figure out how long we will spin
		_spinTime = 1 + ( Mathf.Abs(velocity) * extraSecondsPerVelocity ); 

		// Pause the game (stop all inputs!)
		GameManager.instance.isPaused = true;

		// Start the CoRoutine that updates the circle every frame draw
		StartCoroutine("DoSpin");
	}
		
	private IEnumerator DoSpin () {
		// Store how long we have been spinning
		float currentSpinTime = 0;

		// Store how many degrees to rotate
		float _totalDegreesToRotate;

		if ( !_spinClockwise ) {
			// Degrees to rotate clockwise is our targetRotation minus our current location
			_totalDegreesToRotate = _targetRotation - _currentRotation;

			// If it would make us go backwards, then add one rotation
			if (_totalDegreesToRotate <= 0)
				_totalDegreesToRotate += 360.0f;
		}
		else
		{	
			// Degrees to rotate clockwise is our targetRotation minus our current location
			_totalDegreesToRotate = _targetRotation - _currentRotation;

			// If this is greater than zero, then we would rotate the other way, so subtract 360
			if (_totalDegreesToRotate >= 0)
				_totalDegreesToRotate -= 360.0f;

			// Now, we should have the negative version of how many degrees to move, so make it positive
			_totalDegreesToRotate = Mathf.Abs(_totalDegreesToRotate);

		}

		// Now, add some extra rotations due to velocity
		_totalDegreesToRotate += 360.0f * Mathf.Ceil(extraRotationsPerVelocity * _velocity);

		// Start at having rotated 0 degrees
		float degreesRotatedSoFar = 0;

		//Debug.Log("Started rotating: " +_totalDegreesToRotate+ " degrees over "+ _spinTime +" seconds.");

		while ( currentSpinTime < _spinTime) {
			// Update our timer
			currentSpinTime += Time.deltaTime;

			// Find what percent of the total time we have been spinning, and clamp it to 0 to 1
			float _percentSpinSoFar = currentSpinTime / _spinTime;
			_percentSpinSoFar = Mathf.Clamp01 (_percentSpinSoFar);

			// Find where that is on the curve and get the curve value - use that instead of percent spin so far;
			float curveValue = speedCurve.Evaluate(_percentSpinSoFar);

			// Find out how many degrees I have rotates so far
			degreesRotatedSoFar = (curveValue * _totalDegreesToRotate);

			// Rotate to that rotation
			if ( !_spinClockwise )
			{
				// Move to that rotation
				spinnerPanelTransform.eulerAngles = new Vector3(0, 0, _startRotation + degreesRotatedSoFar);
			}
			else				
			{
				// Move to that rotation
				spinnerPanelTransform.eulerAngles = new Vector3(0, 0, _startRotation - degreesRotatedSoFar);
			}

			// Save current rotation in case we need it
			_currentRotation = spinnerPanelTransform.rotation.eulerAngles.z;

			// Next Frame
			yield return null;
		}


		// Debug if needed
		// Debug.Log ("Spin Ended at: " + _currentRotation + " degrees | " + (GetCurrentPercent () * 100) +"%");

		// Force stop to target
		_currentRotation = _targetRotation;
		spinnerPanelTransform.eulerAngles = new Vector3(0,0,_targetRotation);

		// Call our post spin functions
		OnSpinComplete();

	}

	public float GetCurrentPercent () {
		return Helpers.DegreesToPercent01 (spinnerPanelTransform.rotation.eulerAngles.z);
	}

	public float GetTargetPercent () {
		return Helpers.DegreesToPercent01 (_targetRotation);
	}

	public void OnSpinComplete () {
		// Unpause the Game
		GameManager.instance.isPaused = false;

		// Send results to arMode
		arMode.OnSpinComplete();

		// Set that we are no longer spinning
		isSpinning = false;
	}

}
