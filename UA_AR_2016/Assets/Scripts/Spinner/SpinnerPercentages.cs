﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpinnerPercentages : MonoBehaviour {

	public float startFill;
	public float endFill;

	private Transform tf;
	private Image image;

	private Vector3 currRot;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		image = GetComponent<Image> ();
		currRot = tf.eulerAngles;
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void fillBar(float newStart, float newEnd, Color newColor){
		currRot.z = newStart;  //sets rotation to start fill from
		tf.eulerAngles = currRot;  //updates rotation
		image.fillAmount = newEnd;  //fills in bar from percent (0 to 1)
		image.color = newColor;
	}
}
