﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class SpinFiller : MonoBehaviour {

	public Image runPercentImage;
	public Image catchPercentImage;

	public ArMode ar;

	// Use this for initialization
	void Start () {
		ar = FindObjectOfType<ArMode> ();
	}
	
	// Update is called once per frame
	public void UpdateSpinner () {

		// Draw Catch from 0 to catchChance
		catchPercentImage.fillAmount = ar.catchChance;
		catchPercentImage.color = Color.green;

		// Draw Flee from 0 to fleeChance, then rotate it so it is at the end of catchChance
		runPercentImage.fillAmount = ar.runChance;
		runPercentImage.color = Color.red;
		runPercentImage.transform.eulerAngles = new Vector3 (0,0,Helpers.Percent01ToDegrees(-ar.catchChance));

	}
}
