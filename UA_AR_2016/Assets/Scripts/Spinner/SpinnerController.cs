﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class SpinnerController : MonoBehaviour {

	[Header("Set By Server On Level Load")]
	public float targetPercent;

	[Header("Options")]
	public float degreesPerVelocity = 500;

	// Private variables
	private float _angleThisFrame;
	private float _angleLastFrame;
	private float _moveDelta;
	private float _velocity;

	private bool _isOver = false;
	private bool _isDragging = false;
	private bool _spinClockwise;

	public Transform spinnerPanelTransform;
	private SpinnerMover mover;
	private float distanceToCamera;

	// Use this for initialization
	void Start () {
		// Load components
		mover = GetComponent<SpinnerMover> ();

		// Set defaults
		_isOver = false;
		_isDragging = false;

		// Save distance to camera
		distanceToCamera = Vector3.Distance(Camera.main.transform.position, spinnerPanelTransform.position);
	}

	void Update () {

		// If we are paused, don't do anything
		if (GameManager.instance.Paused) {
			return;
		}


		_angleThisFrame = GetAngleToMouse ();

		if (_isDragging) {
			// Find the movement delta
			_moveDelta =  _angleThisFrame - _angleLastFrame;

			// Velocity is how much I moved per frame / seconds per frame
			_velocity = _moveDelta / Time.deltaTime;
			_velocity /= degreesPerVelocity;

			// But be sure to add our start angle (so that where we clicked to start dragging is important)
			spinnerPanelTransform.Rotate(new Vector3(0f, 0f, _moveDelta));

			// Spinning clockwise if our angle is increasing
			_spinClockwise = (_moveDelta < 0);
		}


		// Save my new positions
		_angleLastFrame = _angleThisFrame;

	}
		
	/// <summary>
	/// Returns the Angles to the mouse.
	/// </summary>
	/// <returns>Angle the to mouse.</returns>
	private float GetAngleToMouse () {
		Vector3 positionInWorldSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distanceToCamera);
		positionInWorldSpace = Camera.main.ScreenToWorldPoint( positionInWorldSpace);

		return LookAtAngleRight ( positionInWorldSpace );
	}


	/// <summary>
	/// Returns the angle rotation needed to get transform.right to look at the point
	/// </summary>
	/// <returns>The at angle.</returns>
	/// <param name="positionInWorldSpace">Position (in world space).</param>
	public float LookAtAngleRight ( Vector3 positionInWorldSpace ) {

		// Find the vector to the position (in world space)
		Vector3 vectorFromPointToObject = positionInWorldSpace - spinnerPanelTransform.position;

		// Find rotation to this object
		float zRotation = Mathf.Atan2 (vectorFromPointToObject.y, vectorFromPointToObject.x);

		// Convert to degrees
		zRotation *= Mathf.Rad2Deg;

		return zRotation + 180;
	}

	void OnMouseEnter () {

		// Track that we are over the ring
		_isOver = true;
	}

	void OnMouseExit () {
		// If we are paused, don't do anything
		if (GameManager.instance.Paused) {
			return;
		}

		_isOver = false;

		if (_isDragging) {
			_isDragging = false;
			CalculateSpin ();
		}			
	}

	void OnMouseUp() {
		// If we are paused, don't do anything
		if (GameManager.instance.Paused) {
			return;
		}

		if (_isDragging) {
			_isDragging = false;
			CalculateSpin ();
		}			

	}

	void OnMouseDown () {
		// If we are paused, don't do anything
		if (GameManager.instance.Paused) {
			return;
		}

		if (_isOver) { 
			_isDragging = true;
		}
	}

	void CalculateSpin () {
		// TODO: Load Random from server? For now: Random value is decimal part of velocity
		targetPercent = _velocity - Mathf.Floor(_velocity);

		// Spin
		mover.Spin (Mathf.Abs(_velocity), Helpers.Percent01ToDegrees(targetPercent), transform.rotation.eulerAngles.z, _spinClockwise);

	}

}
