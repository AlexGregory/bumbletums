﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Status {

	public string id;
	public string displayName;
	public Sprite displayIcon;

}
