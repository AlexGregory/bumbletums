﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ModifiedFloat {

	public float baseValue; 
	public List<Modifier> modifiers;
	public float value {
		get {
			// Start at our base value
			float temp = baseValue;
			// Adjust it for all modifiers the player has
			foreach (Modifier mod in modifiers) {
				// If we have the modifier
				if ( IsStatusEffectOnPlayer(mod.statusID) ) {
					if (mod.method == Modifier.ModifierType.ADD) {
						temp += mod.operand;
					} else if (mod.method == Modifier.ModifierType.MULTIPLY) {
						temp *= mod.operand;
					} else if (mod.method == Modifier.ModifierType.EQUALS) {
						temp = mod.operand;
					}
				}
			}
			// Return the modified value
			return temp;
		}
	}

	private bool IsStatusEffectOnPlayer (string statusID) {
		// Return false if anything isn't loaded
		if (GameManager.instance == null || GameManager.instance.playerData == null || GameManager.instance.playerData.statusEffects == null)
			return false;
		// Otherwise, if we are good to go, search through the modifier for the same ID -- if we have it, return true.
		foreach (Status st in GameManager.instance.playerData.statusEffects) {
			if (st.id == statusID) {
				return true;
			}
		}

		// If we reach the end and don't see it, return false
		return false;
	}

	private bool IsStatusEffectOnPlayer (Status status) {
		return IsStatusEffectOnPlayer (status.id);
	}


	[System.Serializable]	
	public class Modifier {
		public enum ModifierType { ADD, MULTIPLY, EQUALS };
		public string statusID;
		public ModifierType method;
		public float operand;
	}
}
