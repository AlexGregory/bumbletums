﻿
//


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]

public class weatherData : System.Object
{
	
	[System.Serializable]
	public struct Coord
	{
		public double lon;
		public double lat;
	}
	[System.Serializable]

	public class Weather
	{
		public int id;
		public string main;
		public string description;
		public string icon;
	}
	[System.Serializable]

	public struct Main
	{
		public double temp;
		public int pressure;
		public int humidity;
		public double temp_min;
		public double temp_max;
	}
	[System.Serializable]

	public struct Wind
	{
		public double speed;
		public int deg;
	}
	[System.Serializable]

	public struct Clouds
	{
		public int all;
	}
	[System.Serializable]

	public struct Sys
	{
		public int type;
		public int id;
		public double message;
		public string country;
		public int sunrise;
		public int sunset;
	}


		public Coord coord;
		public List<Weather> weather;
		public string @base;
		public Main main;
		public int visibility;
		public Wind wind;
		public Clouds clouds;
		public int dt;
		public Sys sys;
		public int id;
		public string name;
		public int cod;
}