﻿using UnityEngine;
using System.Collections;

public class WeatherGrab : MonoBehaviour 
{
	//Variables for status checkes declaration
	public int barometricPressure;
	public string weatherType;
	public float temperature;
	public float humidity;

	//Temp status declaration
	Status HighTemp = new Status ();
	Status MedTemp = new Status();
	Status LowTemp = new Status ();

	//Barometric pressure status declaration
	Status HighPressure = new Status ();
	Status MedPressure = new Status();
	Status LowPressure = new Status ();

	//Weather Type Status Declaration
	Status Thunderstorm = new Status ();
	Status Drizzle = new Status ();
	Status Rain = new Status ();
	Status Snow = new Status ();
	Status Clear = new Status ();
	Status Clouds = new Status ();

	//Humidity Status Declaratoin
	Status HighHumidity = new Status ();
	Status MedHumidity = new Status();
	Status LowHumidity = new Status ();


	public weatherData weatherdata;




	IEnumerator GrabInfo(float lat, float lon) 
	{
		while (true) {
			string openMapDataURL = "http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&appid=c06643883bc43c77a0b2ae2b92889179"; // Grab the website information 


			WWW openMapWWW = new WWW (openMapDataURL);
			yield return openMapWWW; //grab the website as a www

			//Debug.Log (openMapWWW.text); debug spit out of the info

			string weatherString = openMapWWW.text; // set the json info form the website to a string
			weatherdata = JsonUtility.FromJson<weatherData> (weatherString); // Fill the weatherData object with the JSON information
			//Debug.Log(JsonUtility.ToJson(weatherdata)); Testing stuff

			barometricPressure = weatherdata.main.pressure;
			weatherType = weatherdata.weather [0].main;
			temperature = (float)weatherdata.main.temp;
			humidity = weatherdata.main.humidity;

			yield break;
		}
	}


	// Use this for initialization
	void Start () {
		float lat = 33.376958f;     //Debug info for testint with our current lat/lon
		float lon = -111.97586f;
		StartCoroutine (GrabInfo (lat, lon));

		//Creation of statuses
		HighTemp.id = "WEATHER_HIGHTEMP";
		HighTemp.displayName = "High Temp";
		HighTemp.displayIcon = null;

		MedTemp.id = "WEATHER_MEDTEMP";
		MedTemp.displayName = "Medium Temp";
		MedTemp.displayIcon = null;

		LowTemp.id = "WEATHER_LOWTEMP";
		LowTemp.displayName = "Low Temp";
		LowTemp.displayIcon = null;

		LowPressure.id = "WEATHER_LOWPRESSURE";
		LowPressure.displayName = "Low Pressure";
		LowPressure.displayIcon = null;

		MedPressure.id = "WEATHER_MEDPRESSURE";
		MedPressure.displayName = "Medium Pressure";
		MedPressure.displayIcon = null;

		HighPressure.id = "WEATHER_HIGHPRESSURE";
		HighPressure.displayName = "High Pressure";
		HighPressure.displayIcon = null;

		LowHumidity.id = "WEATHER_LOWHUMIDITY";
		LowHumidity.displayName = "Low Humidity";
		LowHumidity.displayIcon = null;

		MedHumidity.id = "WEATHER_MEDHUMIDITY";
		MedHumidity.displayName = "Medium Humidity";
		MedHumidity.displayIcon = null;

		HighHumidity.id = "WEATHER_HIGHHUMIDITY";
		HighHumidity.displayName = "High Humidity";
		HighHumidity.displayIcon = null;

		Thunderstorm.id = "WEATHER_THUNDERSTORM";
		Thunderstorm.displayName = "Thunderstorm Weather";
		Thunderstorm.displayIcon = null;

		Drizzle.id = "WEATHER_DRIZZLE";
		Drizzle.displayName = "Drizzly Weather";
		Drizzle.displayIcon = null;

		Rain.id = "WEATHER_RAIN";
		Rain.displayName = "Rainy Weather";
		Rain.displayIcon = null;

		Snow.id = "WEATHER_SNOW";
		Snow.displayName = "Snowy Weather";
		Snow.displayIcon = null;

		Clear.id = "WEATHER_CLEAR";
		Clear.displayName = "Clear Weather";
		Clear.displayIcon = null;

		Clouds.id = "WEATHER_CLOUDS";
		Clouds.displayName = "Cloudy Weather";
		Clouds.displayIcon = null;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void weathercheck(float lat, float lon)
	{
		StartCoroutine (GrabInfo (lat, lon));
		//Sending of the Humidity Status
		if(humidity >=0 && humidity < 33)
		{
			GameManager.instance.world.statuses.Add (LowHumidity);
		}
		if(humidity >= 33 && humidity < 67)
		{
			GameManager.instance.world.statuses.Add (MedHumidity);
		}
		if(humidity >=67 && humidity <= 100)
		{
			GameManager.instance.world.statuses.Add (HighHumidity);
		}
		//Barometric pressure statuses
		if(barometricPressure >=0 && barometricPressure < 980)
		{
			GameManager.instance.world.statuses.Add (LowPressure);
		}
		if(barometricPressure >=980 && barometricPressure < 1050)
		{
			GameManager.instance.world.statuses.Add (MedPressure);
		}
		if(barometricPressure >=1050)
		{
			GameManager.instance.world.statuses.Add (HighPressure);
		}

		//Temp statuses
		if(temperature <= 283.15) //If the temp is less than 50 degrees farenheight
		{
			GameManager.instance.world.statuses.Add (LowTemp);
		}
		if(temperature > 283.15 && temperature < 305.372)
		{
			GameManager.instance.world.statuses.Add (MedTemp);
		}
		if(temperature >= 305.372) // if the temp is greater than 90 degrees farenheight.
		{
			GameManager.instance.world.statuses.Add (HighTemp);
		}

		//Weather Statuses

		if(weatherType == "Thunderstorm")
		{
			GameManager.instance.world.statuses.Add (Thunderstorm);
		}

		if(weatherType == "Drizzle")
		{
			GameManager.instance.world.statuses.Add (Drizzle);
		}

		if(weatherType == "Rain")
		{
			GameManager.instance.world.statuses.Add (Rain);
		}

		if(weatherType == "Snow")
		{
			GameManager.instance.world.statuses.Add (Snow);
		}

		if(weatherType == "Clear")
		{
			GameManager.instance.world.statuses.Add (Clear);
		}

		if(weatherType == "Clouds")
		{
			GameManager.instance.world.statuses.Add (Clouds);
		}
	}
}
