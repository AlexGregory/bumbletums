﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DisplayCreatures : MonoBehaviour {

	public List<InstanceCreature> creaturesToDisplay;//list of creatures to display
	public GridLayoutGroup scrollPanel;//panel to child buttons to
	public GameObject buttonPrefab;//prefab for testing
	List<GameObject> displayedObjects;

	// Use this for initialization
	void Start () {
		displayedObjects = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
//		if (Input.GetKeyDown (KeyCode.Space)) {
//			ClearDisplay ();
//			Display (false);
//		}
//		if (Input.GetKeyDown (KeyCode.F)) {
//			ClearDisplay ();
//			Display (true);
//		}
	}

	void ClearDisplay(){
		if (displayedObjects.Count > 0) {
			foreach (GameObject displayed in displayedObjects) {
				//displayedObjects.Remove (displayed);
				Destroy (displayed);
			}
		}
	}

	/// <summary>
	/// Display creatures in scroll panel.
	/// </summary>
	void Display(bool displayFavorites){
		if (displayFavorites) {
			foreach (InstanceCreature animal in creaturesToDisplay) {
				if (animal.isFavorite) {
					GameObject displayButton = Instantiate (buttonPrefab, scrollPanel.gameObject.transform) as GameObject;
					//displayButton.GetComponent<Button> ().image.sprite = animal.creatureThumbnail;

					// Show the personal name, if it exists, else, show the default name
					if (animal.customName != null && animal.customName != "") {
						displayButton.GetComponent<Button> ().GetComponentInChildren<Text> ().text = animal.customName;
					} else {
						displayButton.GetComponent<Button> ().GetComponentInChildren<Text> ().text = animal.sharedData.displayName;
					}

					// Add the button to the display list
					displayedObjects.Add (displayButton);
				}
			}
		}else{
			foreach (InstanceCreature animal in creaturesToDisplay) {
				GameObject displayButton = Instantiate (buttonPrefab, scrollPanel.gameObject.transform) as GameObject;
				// displayButton.GetComponent<Button> ().image.sprite = animal.creatureThumbnail;
				// Show the personal name, if it exists, else, show the default name
				if (animal.customName != null && animal.customName != "") {
					displayButton.GetComponent<Button> ().GetComponentInChildren<Text> ().text = animal.customName;
				} else {
					displayButton.GetComponent<Button> ().GetComponentInChildren<Text> ().text = animal.sharedData.displayName;
				}

				// Add the button
				displayedObjects.Add (displayButton);
			}
		}
	}
}
