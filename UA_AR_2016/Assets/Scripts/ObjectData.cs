﻿using UnityEngine;
[System.Serializable]
public class ObjectData {

	// Use this for initialization
	public GameObject spawnedObject;
	public float lat;
	public float lon;
}
