﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using Facebook.MiniJSON;
using UnityEngine.SceneManagement;
public class FacebookSDK : MonoBehaviour {
	public Text txt;

	public string email;
	//List of permissions sent to Facebook per log in
	List <string> Perms= new List<string>(){"public_profile","email","user_friends"};
	// Use this for initialization
	void Awake()
	{
		//Checks to see if app loads
		if(SceneManager.GetActiveScene().name=="LoginScene" && !FB.IsInitialized)
		{
			FB.Init (InitCompleteCallback, UnityCallbackDelegate);
		}
	}
	public void LoginButton()
	{
		//If user isn't logged in yet
		if (!FB.IsLoggedIn) {
			FB.LogInWithReadPermissions (Perms, LoginCallback);
		} else {
			Debug.LogError ("User is logged in");
			//Shares link and grabs email
			ShareLink ();
		}
	}
	public void LogoutButton()
	{
		Debug.Log ("logout");
		FB.LogOut ();
		GameManager.instance.LoadScene("LoginScene");
	}
			
	private void ShareLink()
	{
		// Make a Graph API call to get email address
		FB.API("/me?fields=email", HttpMethod.GET,  graphResult =>
			{
				if (string.IsNullOrEmpty(graphResult.Error) == false)
				{
					Debug.Log("could not get email address");
					return;
				}

				string emailTemp = graphResult.ResultDictionary["email"] as string;
				//Converts object to string
				email= Json.Serialize(emailTemp);
				GameManager.instance.LoadScene("LoadingTemp");
				//puts email on button(change in future)
				//txt.text=email;
				// Debug.Log(email);
				// Set their userid and token from facebook login
				GameManager.instance.playerData.userid = email;
				GameManager.instance.playerData.token = Facebook.Unity.AccessToken.CurrentAccessToken.TokenString;
			});
		//Link used in share with name and description of link
		//FB.ShareLink (new Uri("https://google.com"),"Bumbletums","Augmented Reality App that allows the user to collect, feed, and become best friends with their bumbletums",null, ShareCallback);
	}

	#region callback

//	Calls back respones to each instance
	private void LoginCallback(IResult result){
		if (result.Cancelled) {
			Debug.LogError ("User cancelled");
		} else {
			Debug.Log ("log in success");

			ShareLink ();
		}
	}
	private void ShareCallback(IShareResult shareResult){

		if (shareResult.Cancelled) {
			Debug.LogError ("User cancelled");
		} else {
			Debug.Log ("share success");
		}
	}
	private void InitCompleteCallback()
	{
		if (FB.IsInitialized) {
			Debug.Log ("Facebook Login Initialized");
		} else {
			Debug.Log ("Facebook Login Failed");
		}

	}

	private void UnityCallbackDelegate(bool isUnity){
		if (isUnity) {
			Time.timeScale = 1;
		} else {
			Time.timeScale = 0;
		}
	}
	#endregion
}
