﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Graveyard {

	public List<GraveyardCreature> objects = new List<GraveyardCreature>();

	public void Add (string UID) {
		// If no time sent, expire an hour from now.
		Add (UID, Helpers.DateTimeToTimestamp (DateTime.Now.AddHours(1)));
	}

	public void Add (string UID, double expiration) {
		if (objects == null) {
			objects = new List<GraveyardCreature>();
		}

		Clean ();
		GraveyardCreature newGYC = new GraveyardCreature ();
		newGYC.UID = UID;
		newGYC.expiration = expiration;
		objects.Add (newGYC);
	}

	private void Clean () {
		if (objects == null) {
			objects = new List<GraveyardCreature>();
		}

		for (int i = 0; i < objects.Count; i++) {
			if (objects [i].expiration >= Helpers.DateTimeToTimestamp (DateTime.Now)) {
				objects.RemoveAt (i);
			}
		}
	}

	public bool Contains (string UID) {

		if (objects == null) {
			return false;
		}

		for (int i = 0; i < objects.Count; i++) {
			if (objects [i].UID == UID) {
				return true;
			}
		}

		return false;
	}

}

[System.Serializable]
public struct GraveyardCreature {
	public string UID;
	public double expiration;
}