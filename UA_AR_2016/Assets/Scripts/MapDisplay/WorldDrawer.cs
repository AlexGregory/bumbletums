﻿using UnityEngine;
using System;
using System.Collections;

public class WorldDrawer : MonoBehaviour {

	public Transform CreatureParentTf;
	public GameObject worldPlayerVisuals;
	public GoogleMapDisplay mapDisplay;
	public ArrowIndicatorManager arrowManager;

	// Use this for initialization
	void Start () {
		ResetWorld ();
	}
	
	// Update is called once per frame
	void Update () {
		// Make the player face in the right direction
		worldPlayerVisuals.transform.rotation = Quaternion.Euler(0, -Input.compass.trueHeading, 0);
	}

	public void ResetWorld() {
		ClearScene ();
		ExpireActors ();
		ShowAvatar ();
		ShowActors ();
	}

	public void ShowAvatar () {
		if (GameManager.instance.playerData.currentAvatar != null) {

			// Spawn avatar - make sure it has no collider
			GameObject temp = GameManager.instance.playerData.currentAvatar.instanceData.CreateActor(worldPlayerVisuals.transform.position, worldPlayerVisuals.transform.rotation, worldPlayerVisuals.transform.parent) as GameObject;
			Destroy (temp.GetComponent<Collider> ());

			// Destroy the old visuals and make this one the new visuals
			Destroy (worldPlayerVisuals);
			temp.name = "Player Avatar ("+GameManager.instance.playerData.currentAvatar.displayName+")";
			worldPlayerVisuals = temp;
		}

	}

	public void ExpireActors () {
        World world = GameManager.instance.world;
        
		for (int i = 0; i < world.creatures.Count; i++) {
            InstanceCreature creature = world.creatures[i];

			if (creature.expiration < Helpers.DateTimeToTimestamp(DateTime.UtcNow)) {
				Debug.Log (creature.sharedData.displayName + " has expired at " + creature.expiration + " : " + Helpers.DateTimeToTimestamp(DateTime.UtcNow));
			}
		}
	}

	// TODO: CALL THIS WHEN OUR PLAYER MOVES
	public void OnPlayerMove() {

		// if the player moves, recreate and draw the arrows
		arrowManager.CreateArrows();
		arrowManager.UpdateArrows ();

		// TODO: Reload the map

	}


	public void ShowActors () {

		// Expire all expired objects!
		ExpireActors();

		// Draw Creatures
		for (int i=0; i<GameManager.instance.world.creatures.Count; i++) {

			// if we don't have it captured already
			if (GameManager.instance.playerData.creatureInv.GetIndexFromUID (GameManager.instance.world.creatures [i].UID) == -1) { 

				// And it's not in the graveyard
				PlayerData pd = GameManager.instance.playerData;
				Graveyard graveyard = pd.graveyard;
				if (!graveyard.Contains (GameManager.instance.world.creatures [i].UID)) {
					// Add it to the world
					GameObject temp = GameManager.instance.world.creatures [i].CreateActor (Helpers.GPSLocationToWorldLocation (GameManager.instance.world.creatures [i].location), Quaternion.identity, CreatureParentTf);
					// Rotate them a random amount
					temp.transform.Rotate(new Vector3(0, UnityEngine.Random.Range(-179,179), 0));
				}
			}
		}

		// Draw Beacons
		for (int i=0; i<GameManager.instance.world.beacons.Count; i++) {
			GameManager.instance.world.beacons [i].CreateActor (Helpers.GPSLocationToWorldLocation (GameManager.instance.world.beacons[i].location), Quaternion.identity, CreatureParentTf);
		}

		// Draw Food
		for (int i=0; i<GameManager.instance.world.foods.Count; i++) {

			// if we don't have it captured already
			//Debug.Log("Checking if "+GameManager.instance.world.foods [i].UID+" is in inventory.");
			if (GameManager.instance.playerData.foodInv.GetIndexFromUID (GameManager.instance.world.foods [i].UID) == -1) { 
				Debug.Log(GameManager.instance.world.foods [i].UID+" is NOT in inventory." + GameManager.instance.playerData.foodInv.GetIndexFromUID (GameManager.instance.world.foods [i].UID));
				GameManager.instance.world.foods [i].CreateActor (Helpers.GPSLocationToWorldLocation (GameManager.instance.world.foods [i].location), Quaternion.identity, CreatureParentTf);
			}
		}

		// Draw the Arrows
		arrowManager.CreateArrows();
		arrowManager.UpdateArrows ();

	}


	public void ClearScene () {
		for (int i = 0; i < CreatureParentTf.childCount; i++) {
			Destroy (CreatureParentTf.GetChild(i).gameObject);
		}
	}
}
