﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class World {

	public List<InstanceCreature> creatures;
	public List<InstanceFood> foods;
	public List<InstanceBeacon> beacons;
	public List<Status> statuses;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
