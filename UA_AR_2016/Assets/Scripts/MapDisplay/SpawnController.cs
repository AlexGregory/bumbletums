﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;



public class SpawnController : MonoBehaviour {

	public WorldDrawer worldDrawer;
	public float randomFoodSpawnTime;
	double nextSpawn;

	public void Start () {
		if (worldDrawer == null) {
			worldDrawer = GetComponent<WorldDrawer> ();
		}

		if (worldDrawer == null) {
			worldDrawer = FindObjectOfType <WorldDrawer> ();
		}

		if (worldDrawer == null) {
			Debug.LogError("ERROR: Could not find World Drawer");
		}

		// Set a random time for the next food to spawn
		randomFoodSpawnTime = Time.time + UnityEngine.Random.Range (GameManager.instance.minFoodSpawnTime.value, GameManager.instance.maxFoodSpawnTime.value);
	}

	public void Update () {
		// Cheats and Hacks!
		if (Input.GetKeyDown (KeyCode.Space)) {
			StartCoroutine (AttemptSpawn ());
		}
		if (Input.GetKeyDown (KeyCode.Z)) {
			Debug.Log (GameManager.instance.playerData.statusEffects);
		}


		// If it is time to attempt a creature spawn
		nextSpawn = GameManager.instance.playerData.lastSpawnTimestamp + GameManager.instance.minSpawnDelay.value;
		if ( Helpers.DateTimeToTimestamp(DateTime.UtcNow) >= nextSpawn) {
			// If so, attempt to spawn and increase 
			StartCoroutine (AttemptSpawn ());
			GameManager.instance.playerData.lastSpawnTimestamp = Helpers.DateTimeToTimestamp (DateTime.UtcNow);
		}


		// Food spawn is random from min to max time. This is NOT saved, it is reset when the game restarts, so it could be forever if they keep quitting the game.
		if (Time.time > randomFoodSpawnTime) {

			// Spawn a food
			StartCoroutine (AttemptFoodSpawn ());

			// Set a random time for the next food to spawn
			randomFoodSpawnTime = Time.time + UnityEngine.Random.Range (GameManager.instance.minFoodSpawnTime.value, GameManager.instance.maxFoodSpawnTime.value);
		}

	}



	public IEnumerator AttemptFoodSpawn (){
		Debug.Log ("Attempting to spawn food...");

		// Choose a random food
		InstanceFood newFoodInstance = ChooseWeightedFood ();

		// Choose a random location
		newFoodInstance.location = Helpers.RandomLocationInRangeGPS(GameManager.instance.foodMaxSpawnDistance.value);

		// Save that world object to the server
		Debug.Log ("Saving world...");
		yield return StartCoroutine (DataManager.instance.DoFoodSave(newFoodInstance));

		// Load new world
		Debug.Log ("Loading world...");
		yield return StartCoroutine(DataManager.instance.DoWorldLoad(GameManager.instance.playerLocation));	

		// Reload the world after we spawn
		worldDrawer.ResetWorld();

		Debug.Log ("Food spawn complete...");
	}


	public IEnumerator AttemptSpawn () {
		Debug.Log ("Attempting to Spawn");

		// If I have not spawned too many
		if (!HaveSpawnedTooMany ()) {

			// Do the spawn
			yield return StartCoroutine (DoSpawnRandomCreature ());
						 
		} else {
			Debug.Log ("Could not spawn - already too many in zone.");
		}
	}

	public IEnumerator DoSpawnRandomCreature () {

		Debug.Log ("Start Spawning Random Creature");

		// Create a world object of a random creature at that location
		InstanceCreature newCreatureInstance = ChooseWeightedCreature();

		yield return StartCoroutine (DoSpawn (newCreatureInstance));

	}

	public IEnumerator DoSpawn ( InstanceCreature newCreatureInstance ) {

		Debug.Log ("Start Spawning Creature: " + newCreatureInstance.sharedData.id);

		newCreatureInstance.location = Helpers.RandomLocationInRangeGPS(GameManager.instance.creatureMaxSpawnDistance.value);

		// Save spawn time
		GameManager.instance.playerData.lastSpawnTimestamp = Helpers.DateTimeToTimestamp(DateTime.UtcNow);

		// Save that world object to the server
		Debug.Log ("Saving world...");
		yield return StartCoroutine (DataManager.instance.DoCreatureSave(newCreatureInstance));

		// Save our player's spawn time
		Debug.Log ("Saving player (last spawn time)...");
		yield return StartCoroutine (DataManager.instance.DoPlayerSave());

		// Load new world
		Debug.Log ("Loading world...");
		yield return StartCoroutine(DataManager.instance.DoWorldLoad(GameManager.instance.playerLocation));	

		// Reload the world after we spawn
		worldDrawer.ResetWorld();
	}


	public bool HaveSpawnedTooMany () {

		// Check if we have spawned too many
		if (GameManager.instance.world.creatures.Count >= GameManager.instance.maxCreaturesInWorld) {
			Debug.Log("Could not spawn. Already "+GameManager.instance.world.creatures.Count+" in scene total.");			
			return true;
		}

		// Check if we have PERSONALLY spawned to many
		int numPersonalSpawns = 0;
		for (int i = 0; i < GameManager.instance.world.creatures.Count; i++) {
			if (GameManager.instance.world.creatures [i].owner == GameManager.instance.playerData.userid) {
				numPersonalSpawns++;
			}
		}
		if (numPersonalSpawns > GameManager.instance.maxPersonalSpawns.value) {
			Debug.Log("Could not spawn. Already "+numPersonalSpawns+" in scene that belong to us.");			
			return true;
		}

		// If not, then we can spawn one
		return false;
	}


	public InstanceFood ChooseWeightedFood () {

		// start with an empty list
		List<ActorFood> spawnActorList = new List<ActorFood>();

		// Loop through creatures in Data Manager
		foreach (ActorFood temp in DataManager.instance.FoodList) {
			// Look at the weight of their spawning
			int foodWeight = Mathf.CeilToInt(temp.spawnChance.value);
			// put that many of them in the list
			for (int i = 0; i < foodWeight; i++) {
				spawnActorList.Add (temp);
			}
		}

		// Turn random actor (prefab) into a new instance (data only object)
		ActorFood selectedActor = spawnActorList [UnityEngine.Random.Range (0, spawnActorList.Count)];
		InstanceFood newFood = new InstanceFood (selectedActor);
		newFood.location = new GPSLocation ();
		newFood.owner = GameManager.instance.playerData.userid;

		// Choose a random creature from that list and return a new Instance of it
		return newFood;
	}



	public InstanceCreature ChooseWeightedCreature () {

		// start with an empty list
		List<ActorCreature> spawnActorList = new List<ActorCreature>();

		// Loop through creatures in Data Manager
		foreach (ActorCreature temp in DataManager.instance.CreatureList) {
			// Look at the weight of their spawning
			int creatureWeight = Mathf.CeilToInt(temp.spawnChance.value);
			// put that many of them in the list
			for (int i = 0; i < creatureWeight; i++) {
				spawnActorList.Add (temp);
			}
		}

		// Turn random actor (prefab) into a new instance (data only object)
		ActorCreature selectedActor = spawnActorList [UnityEngine.Random.Range (0, spawnActorList.Count)];
		InstanceCreature newCreature = new InstanceCreature (selectedActor);

		newCreature.location = new GPSLocation ();


		// Choose a random creature from that list and return a new Instance of it
		return newCreature;
	}


}
