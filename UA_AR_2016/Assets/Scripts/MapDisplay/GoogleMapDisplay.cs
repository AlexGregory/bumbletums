﻿using UnityEngine;
using System.Collections;


public class GoogleMapDisplay :  MonoBehaviour {

	[Header("GameObjects")]
	public GameObject mapObject;

    public static class ColorTypeConverter
    {
        public static string ToRGBHex(Color c)
        {
            return string.Format("{0:X2}{1:X2}{2:X2}", ToByte(c.r), ToByte(c.g), ToByte(c.b));
        }

        private static byte ToByte(float f)
        {
            f = Mathf.Clamp01(f);
            return (byte)(f * 255);
        }
    }

    public enum MapType
    {
        RoadMap,
        Satellite,
        Terrain,
        Hybrid
    }

    [Header ("Test Location Area")]
    public GoogleMapLocation centerLocation;

    [Header("Map Colors")]
    [Header("  -ALL-  ")]
    public bool textVisibility = false;
    public bool iconVisibility = false;
    [Header("  -Administrative- ")]
    public bool geometryVisibility = false;
    public Color countryColor;
    public Color provinceColor;
    public Color localityColor;
    public Color neighborhoodColor;
    public Color land_parcelColor;
    [Header("  -Landscape- ")]
    public Color manMadeColor;
    public Color naturalLandcoverColor;
    public Color naturalTerrainColor;
    [Header("  -Point of Interests- ")]
    public Color attractionColor;
    public Color businessColor;
    public Color governmentColor;
    public Color medicalColor;
    public Color parkColor;
    public Color place_of_worshipColor;
    public Color schoolColor;
    public Color sports_complexColor;
    [Header("  -Road- ")]
    public Color roadFillColor;
    public Color roadStrokeColor;
    public Color highwayFillColor;
    public Color highwayStrokeColor;
    [Header("  -Transit- ")]
    public Color lineFillColor;
    public Color stationFillColor;
    public Color airportFillColor;
    public Color bus_stationFillColor;
    public Color rail_stationFillColor;

    [Header("  -Landscape- ")]
    public Color waterFillColor;

    [Header ("Map Options")]
    public MapType mapType;
    public string APIKey = "AIzaSyCTPE_0S3YLmcx7t3HNWzntIsWcH8c0vBE"; //This is my personal key, tied to my google account. When it runs out, you can just place a new key in the inspector to replace mine 
    [Tooltip("Google's Scale-  1: World  5: Landmass/Continent  10: City  15: Streets  20: Buildings")]
    [Range(0,20)]
    public int zoom = 20;                       //Zoom levels range from 1 - 20. Here's google's scale.     1: World    5: Landmass/Continent    10: City    15: Streets    20: Buildings
    public int size = 512;                      //Size of the image requested from google in pixels. 512x512 is the default resolution size.
    [Range(1, 2)]
    public int scale = 1; 
    public bool doubleResolution = false;
    //private float timer = 5.0f;
    public float timeForRefresh;               //Refresh the map in slower intervals, so we don't reach the google static maps limit as quickly.
	private float timeUp;

    // Use this for initialization
    void Start () {
		FullMapUpdate ();

		timeUp = Time.fixedTime + timeForRefresh;
    }
	
	// Update is called once per frame
	void FixedUpdate () {

		if (Time.fixedTime >= timeUp) {
			FullMapUpdate ();
			timeUp = Time.fixedTime + timeForRefresh;
		}

        //TODO:: Create a reasonable refresh rate. So store locations locally, and only update if the user moves a generous amount from their previous location. 

        //Really bad refresh code
        //timer -= Time.deltaTime;
        //if (timer <= 0f)
        //{
        //StartCoroutine(RefreshMapDisplay());
        // timer = timeForRefresh;
        //}

    }

	public void LocationUpdate () {
		GameManager.instance.StartCoroutine("LocationService"); //Ping for device location before attempting to even display the map. 
	}

	public void MapUpdate () {
		StartCoroutine(RefreshMapDisplay()); //Obtain the map texture
	}

	public void FullMapUpdate() {
		StartCoroutine (DoFullMapUpdate ());
	}

	IEnumerator DoFullMapUpdate () {
		yield return GameManager.instance.StartCoroutine("LocationService"); //Ping for device location before attempting to even display the map.

		centerLocation.latitude = (float)GameManager.instance.playerLocation.latitude;
		centerLocation.longitude = (float)GameManager.instance.playerLocation.longitude;

		yield return StartCoroutine(RefreshMapDisplay()); //Obtain the map texture
	}



    IEnumerator RefreshMapDisplay()  //This is where the actual google static map API is used.  
    {
		Debug.Log ("New Refresh Running.");

        var url = "http://maps.googleapis.com/maps/api/staticmap";  //Base URL request for google static maps API, rest of the code adds onto the base url to get an image of the map at the user's location.
        var ext = "";

        // This is where the location data that has been retrieved from the user's phone is added onto the string of the base static maps url.

        ext += "center=" + WWW.UnEscapeURL(string.Format("{0},{1}", centerLocation.latitude, centerLocation.longitude)); //WWW.UnEscapeURL converts strings into URL format. 

        // This section here is where all the styling for the map is done. Color elements can be changed, opacity can be changed, icons can be disabled etc etc. It follow the format below. 
        // I hard coded just some example stuff because Hue told me the disigners would be using another tool to style our maps. Thus I commented out this section but this is where that
        // information would go or whatever we'd need to build to implement that said tool would go. I think it's called like style maps? if you simply google it.
        //  
        //style=feature:myFeatureArgumentelement:myElementArgument%7CmyRule1:myRule1Argument%7CmyRule2:myRule2Argument
        //
        //
        //Here are the style elements

        ext += "&style=feature:all%7Celement:labels.text%7Cvisibility:" + (textVisibility ? "on" : "off"); 
        ext += "&style=feature:all%7Celement:labels.icon%7Cvisibility:" + (iconVisibility ? "on" : "off");
        ext += "&style=feature:administrative.all%7Celement:geometry%7Cvisibility:" + (geometryVisibility ? "on" : "off");
        ext += "&style=feature:administrative.country%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(countryColor).ToLower();
        ext += "&style=feature:administrative.province%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(provinceColor).ToLower();
        ext += "&style=feature:administrative.locality%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(localityColor).ToLower();
        ext += "&style=feature:administrative.land_parcel%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(land_parcelColor).ToLower();
        ext += "&style=feature:landscape.man_made%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(manMadeColor).ToLower();
        ext += "&style=feature:landscape.natural.landcover%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(naturalLandcoverColor).ToLower();
        ext += "&style=feature:landscape.natural.terrain%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(naturalTerrainColor).ToLower();
        ext += "&style=feature:poi.attraction%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(attractionColor).ToLower();
        ext += "&style=feature:poi.business%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(businessColor).ToLower();
        ext += "&style=feature:poi.government%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(governmentColor).ToLower();
        ext += "&style=feature:poi.medical%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(medicalColor).ToLower();
        ext += "&style=feature:poi.park%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(parkColor).ToLower();
        ext += "&style=feature:poi.place_of_worship%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(place_of_worshipColor).ToLower();
        ext += "&style=feature:poi.school%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(schoolColor).ToLower();
        ext += "&style=feature:poi.sports_complex%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(sports_complexColor).ToLower();
        ext += "&style=feature:road%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(roadFillColor).ToLower();
        ext += "&style=feature:road%7Celement:geometry.stroke%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(roadStrokeColor).ToLower();
        ext += "&style=feature:road.highway%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(highwayFillColor).ToLower();
        ext += "&style=feature:road.highway%7Celement:geometry.stroke%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(highwayStrokeColor).ToLower();
        ext += "&style=feature:transit.line%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(lineFillColor).ToLower();
        ext += "&style=feature:transit.station%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(stationFillColor).ToLower();
        ext += "&style=feature:transit.station.airport%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(airportFillColor).ToLower();
        ext += "&style=feature:transit.station.bus%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(bus_stationFillColor).ToLower();
        ext += "&style=feature:transit.station.rail%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(rail_stationFillColor).ToLower();
        ext += "&style=feature:water%7Celement:geometry.fill%7Ccolor:0x" + ColorTypeConverter.ToRGBHex(waterFillColor).ToLower();

        //Here are the other elements that you can change in the inspector. 

        ext += "&zoom=" + zoom.ToString();
        ext += "&size=" + WWW.UnEscapeURL(string.Format("{0}x{0}", size));
        ext += "&scale=" + (doubleResolution ? "2" : "1");
        ext += "&maptype=" + mapType.ToString().ToLower();
        ext += "&key=" + APIKey;

        var request = new WWW(url + "?" + ext); //This line creates the whole URL we incrementally built above and stores it in a variable named request.

        //Debug.Log(url + "?" + ext);

		// Wait for map to load
        yield return request;

		// Update the texture
        mapObject.GetComponent<Renderer>().material.mainTexture = request.texture; // Takes the renderer component of the object this script is attached to, and applies the texture (the image google static map provides) and applies the image to the object as a texture.
    }

    [System.Serializable]
    public class GoogleMapLocation
    {
        public string address;
        public float latitude;
        public float longitude;
    }

   


}


