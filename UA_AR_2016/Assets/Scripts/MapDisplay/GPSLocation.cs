﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class GPSLocation {
	public double latitude;
	public double longitude;
	public double altitude;
}
