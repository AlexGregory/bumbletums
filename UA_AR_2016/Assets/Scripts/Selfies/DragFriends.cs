﻿using UnityEngine;
using System.Collections;

public class DragFriends : MonoBehaviour {

    float distance = 43;
   


	void OnMouseDrag()
    {

        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
        Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
		objPosition.z = distance;

        transform.position = objPosition;
    }
}
