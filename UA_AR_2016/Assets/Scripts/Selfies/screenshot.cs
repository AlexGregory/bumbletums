﻿using UnityEngine;
using System.Collections;
using System.IO;

public class screenshot : MonoBehaviour {

	/*
	public void TakePicture(){
		string screenShotName = "MeAndMyFriend" + System.DateTime.Now.ToString ("_yyyy-MM-HH-mm") + ".png";
		Debug.Log (Application.persistentDataPath);
		Application.CaptureScreenshot (screenShotName);

	}
	
}
*/


Texture2D screenCap;
	Texture2D border;
	bool shot = false;

	/*
	// Use this for initialization
	void Start () {
		//screenCap = new Texture2D (300,200,TextureFormat.RGB24,false);
		//border = new Texture2D (2,2,TextureFormat.ARGB32,false);
		//border.Apply ();

	}
	
	// Update is called once per frame
	//void Update () {
	//	if (Input.GetKeyUp (KeyCode.Space)) {
	//		//Application.CaptureScreenshot ("MyFriend.png");
	//		StartCoroutine ("Capture");
	//	}
	//}
	/*

	void OnGUI(){
		GUI.DrawTexture(new Rect(200,100,300,2),border, ScaleMode.StretchToFill);	//top
		GUI.DrawTexture(new Rect(200,300,300,2),border, ScaleMode.StretchToFill);	//bottom
		GUI.DrawTexture(new Rect(200,100,2,200),border, ScaleMode.StretchToFill);	//left
		GUI.DrawTexture(new Rect(500,100,2,200),border, ScaleMode.StretchToFill);	//right
	
		if (shot == true) {
			GUI.DrawTexture (new Rect(10,10,60,40),screenCap,ScaleMode.StretchToFill);
		}
	
	}
	
*/
	public void TakePicture(){
		string screenShotName = "MeAndMyFriend" + System.DateTime.Now.ToString ("_yyyy-MM-HH-mm") + ".png";
		Application.CaptureScreenshot (screenShotName);

		string originPath = System.IO.Path.Combine(Application.persistentDataPath, screenShotName);

		string path = "/mnt/sdcard/" + screenShotName;
		if (File.Exists (originPath)) {
			File.Move (originPath, path);
		}
	}
		
}