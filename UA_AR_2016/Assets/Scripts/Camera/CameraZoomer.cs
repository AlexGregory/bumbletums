﻿using UnityEngine;
using System.Collections;

public class CameraZoomer : MonoBehaviour
{

    //THROW THIS SCRIPT ONTO THE CAMERA YOU WANT TO ZOOM WITH

    public GameObject ObjectToOrbit;
    public Camera cameraObject;

    public float minZoom = .5f; //Min zoom distance of the camera from the target
    public float maxZoom = 1.5f;

    public float radius = 2.0f; //The Current Distance Away from the object it's orbiting.
    public float zoomSpeed = 0.5f;
    public float orthoZoomSpeed = .5f;

    private GameObject target;
    private Vector3 desiredPosition;

    void Start()
    {

		radius = Mathf.Floor((minZoom + minZoom)/2);
        target = ObjectToOrbit;
        cameraObject = gameObject.GetComponent<Camera>();
        transform.position = (transform.position - target.transform.position).normalized * radius + target.transform.position;

    }

    void Update()
    {

        radius -= Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;
        radius = Mathf.Clamp(radius, minZoom, maxZoom);

              
        desiredPosition = (transform.position - target.transform.position).normalized * radius + target.transform.position;
                

		transform.position = Vector3.MoveTowards(transform.position, desiredPosition,  (zoomSpeed * Time.deltaTime));


        if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            float deltaMagnitudediff = prevTouchDeltaMag - touchDeltaMag;
            if (cameraObject.orthographic)
            {
                cameraObject.orthographicSize += deltaMagnitudediff * orthoZoomSpeed;
                cameraObject.orthographicSize = Mathf.Max(cameraObject.orthographicSize, .1f);
            }
            else
            {
                cameraObject.fieldOfView += deltaMagnitudediff * zoomSpeed;
                cameraObject.fieldOfView = Mathf.Clamp(cameraObject.fieldOfView, 5f, 75f);
            }
        }


    }

}
