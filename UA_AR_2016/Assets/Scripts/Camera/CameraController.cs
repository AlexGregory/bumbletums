﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    //Throw this script onto the PARENT game object, of whatever camera you want to orbit with. Make sure that the game object that this script is attached to, is also where you want to orbit around.
    //For Instance, if you want your camera to orbit around the player. Put this script onto the player game object. Then, make the camera a child of said object. 


    #region Public Variables
    public float minYRotation = 0f;
    public float maxYRotation = 50f;
    #endregion

    #region Private References
    [SerializeField, Range(0.0f, 1.0f)]
    private float _lerpRate;
    private float _xRotation;
    private float _yYRotation;
    private Vector3 desiredPosition;
    #endregion

    #region Private Methods
    private void Rotate(float xMovement, float yMovement)
    {
        _xRotation += xMovement;
        _yYRotation += yMovement;
    }
    #endregion

    #region Unity Callbacks
    void Start () {
        InputManager.MouseMoved += Rotate;

	}
	
	void Update () {

        _xRotation = Mathf.Lerp(_xRotation, 0, _lerpRate);
        _yYRotation = Mathf.Lerp(_yYRotation, 0, _lerpRate);


        Vector3 objRotation = transform.rotation.eulerAngles;
        float clampedYRotation = Mathf.Clamp(objRotation.x, minYRotation, maxYRotation);
        transform.eulerAngles = new Vector3(clampedYRotation, objRotation.y , objRotation.z);

        transform.eulerAngles += new Vector3(-_yYRotation, _xRotation, 0);


    }

    void OnDestroy()
    {
        InputManager.MouseMoved -= Rotate;
    }
    #endregion
}
