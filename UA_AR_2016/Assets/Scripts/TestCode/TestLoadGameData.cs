﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TestLoadGameData : MonoBehaviour {
	public InputField email;
	public InputField token;

	public GPSLocation debugLocation;
	public GPSLocation currentLocation;

	// Use this for initialization
	void Start () {
		

	}
	
	// Update is called once per frame
	void Update () {
	}

	public void DebugLogin () {
		GameManager.instance.playerData.userid = email.text; 
		GameManager.instance.playerData.token = token.text;

		if (!Input.location.isEnabledByUser || Input.location.status != LocationServiceStatus.Running) {
			currentLocation = debugLocation;
		} else {
			currentLocation.latitude = Input.location.lastData.latitude;
			currentLocation.longitude = Input.location.lastData.longitude;
			// Note: Change to ignore altitude
			currentLocation.altitude = 0.0;
			//currentLocation.altitude = Input.location.lastData.altitude;
		}

		StartCoroutine ("DoFullLogin");
	}


	public void TestLogin () {
		GameManager.instance.playerData.userid = "hue@huehenry.com"; 
		GameManager.instance.playerData.token = "MyMommaDontLikeThisTokenAndSheLikesEveryone";

		if (!Input.location.isEnabledByUser || Input.location.status != LocationServiceStatus.Running) {
			currentLocation = debugLocation;
		} else {
			currentLocation.latitude = Input.location.lastData.latitude;
			currentLocation.longitude = Input.location.lastData.longitude;
			// Note: Change to ignore altitude
			currentLocation.altitude = 0.0;
			//currentLocation.altitude = Input.location.lastData.altitude;
		}

		StartCoroutine ("DoFullLogin");
	}


	private IEnumerator DoFullLogin () {

		yield return DataManager.instance.StartCoroutine ("DoPlayerLogin");
		yield return DataManager.instance.StartCoroutine ("DoPlayerLoad");
		yield return DataManager.instance.StartCoroutine ("DoWorldLoad", currentLocation);

		GameManager.instance.LoadScene("MainScene");
	}



}
