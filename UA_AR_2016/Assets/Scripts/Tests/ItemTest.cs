﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ItemTest : MonoBehaviour {

	public ActorCreature[] creatures;
	public List<InstanceCreature> pretendWorldData;

	public InputField lat;
	public InputField lon;
	public InputField alt;

	// Use this for initialization
	void Start () {
		// Load all 
		creatures = Resources.LoadAll<ActorCreature> ("Bumbletums/");

		lat.text = GameManager.instance.playerLocation.latitude.ToString();
		lon.text = GameManager.instance.playerLocation.longitude.ToString();
		alt.text = GameManager.instance.playerLocation.altitude.ToString();

	}
	
	// Update is called once per frame
	void Update () {
	}

	public void GoToUAT () {
		GameManager.instance.playerLocation.latitude = 33.37770950000001;
		GameManager.instance.playerLocation.longitude = -111.9759985;             //Set to UAT Coords
		GameManager.instance.playerLocation.altitude = 371.059;

		GameManager.instance.LoadScene ("MainScene");
	}

	public void PrintInventory ( ) {
		Debug.Log ("INVENTORY");
		for (int i = 0; i < GameManager.instance.playerData.creatureInv.ItemCount; i++) {
			Debug.Log (GameManager.instance.playerData.creatureInv.GetItem(i) + "|" + GameManager.instance.playerData.creatureInv.GetItem(i).sharedData.id );
		}
	}

	public void GimmeABeacon () {
		int randomize = Random.Range (0, DataManager.instance.BeaconList.Length); //gimme a random beacon -Cooper 12-15
		GameManager.instance.playerData.beaconInv.AddItem (new InstanceBeacon(DataManager.instance.BeaconList [randomize]));
	}

	public void GimmeAFood () {
		int randomize = Random.Range (0, DataManager.instance.FoodList.Length);  //gimme a random food -Cooper 12-14
		GameManager.instance.playerData.foodInv.AddItem (new InstanceFood(DataManager.instance.FoodList [randomize]));
	}

	public void UpdateLocation () {
		GameManager.instance.playerLocation.latitude = double.Parse(lat.text);
		GameManager.instance.playerLocation.longitude = double.Parse(lon.text);
		GameManager.instance.playerLocation.altitude = double.Parse(alt.text);

		GameManager.instance.LoadScene ("MainScene");
	}

	public void TestLogin () {
		GameManager.instance.playerData.userid = "hue@huehenry.com"; 
		GameManager.instance.playerData.token = "MyMommaDontLikeThisTokenAndSheLikesEveryone";

		StartCoroutine ("DoFullLogin");
	}


	private IEnumerator DoFullLogin () {

		yield return DataManager.instance.StartCoroutine ("DoPlayerLogin");
		yield return DataManager.instance.StartCoroutine ("DoPlayerLoad");
		yield return DataManager.instance.StartCoroutine ("DoWorldLoad", GameManager.instance.playerLocation);

		GameManager.instance.LoadScene("MainScene");
	}
}
