﻿using UnityEngine;
using System.Collections;

public class Spin : MonoBehaviour {

	[Tooltip("Spin around each axis in angles per second")] public Vector3 spin;
	private Transform tf;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		// spin
		tf.Rotate (spin * Time.deltaTime);	
	}
}
