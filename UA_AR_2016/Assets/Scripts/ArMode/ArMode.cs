﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class ArMode : MonoBehaviour {

	#region Inspector Fields

	[Header("Inspector Fields")]
	[SerializeField]
	private ActorCreature defaultCreature;	//Default creature will be loaded in if the game does not have one in the scene
	[SerializeField]
	private RawImage arImage;
	[SerializeField]
	private GameObject defaultWorld;
	[SerializeField]
	private SpinnerMover spinner;

	[Header("Animations")]
	public Animator introAnimation;
	public Animator runawayAnimation;
	public Animator catchAnimation;
	public Animator tryAgainAnimation;

	#endregion

	#region Components and Other GameObjects

	[Header("Components")]
	private Transform cameraTf;
	private GyroCamera gyroCamera;

	public GameObject creatureGameObject; 
	private Renderer creatureRenderer;

	public Text TEMP_ValueDisplayText;

	public FoodController foodController;
	#endregion

	#region Spinner Variables
	[Header("Spinner Variables")]
	public float catchChance;
	public float runChance;
	public float tryAgainChance;

	public float spinnerWillLandOn;
	#endregion


	#region Inspector Variables
	[Header("Inspector Variables")]
	[SerializeField]
	//Shows that the device has a gyroscope
	private Material hasGyroscopeMaterial;
	[SerializeField]
	//Shows that the device does not have a gyroscope
	private Material noGyroscopeMaterial; 
	// Holds the default position/rotation/scale for a BT
	public Transform prototypeBumbletum;
	// is AR Mode?
	public bool isARMode;
	#endregion

	#region MonoBehaviour Methods
	void Awake() {
		//Get the components
		gyroCamera = GetComponent<GyroCamera> ();
		prototypeBumbletum.gameObject.SetActive (false);
	}

	// Use this for initialization
	void Start () {

		// Start in a paused state
		GameManager.instance.isPaused = true;

		// Play intro animation
		introAnimation.SetTrigger("SlideIn");

		//Checks if the device has a gyroscope or not and switches mode
		if (!SystemInfo.supportsGyroscope) {
			UpdateMode (false);
			Debug.LogWarning ("Your phone does not support a gyroscope.");
		} else {
			UpdateMode (true);
		}

		// After this is done, wait and unpause
		StartCoroutine("StartCollectionMode");
	}

	public IEnumerator StartCollectionMode () {

		// Wait a frame, so everything else is 
		yield return null;

		// Load our selected creature's data
		InstanceCreature creatureScript = GameManager.instance.selectedCreature;

		//Load in default creature if the game cannot find one to use
		if (creatureScript.sharedData == null) {
			// If there is a default creature...
			if (defaultCreature != null) {
				Debug.LogWarning ("Could not find creature! Creating a default one.");
				creatureScript = defaultCreature.instanceData;
				GameManager.instance.selectedCreature = defaultCreature.instanceData;
			} else {
				// otherwise, no default creature, so return to main scene
				Debug.LogError ("Could not find creature or a default one!");
				GameManager.instance.Paused = false;
				yield break;
			}
		} 

		creatureGameObject = creatureScript.CreateActor(Vector3.zero, Quaternion.identity);

		//Ensure the creature is scaled for ArMode
		creatureGameObject.transform.localScale = prototypeBumbletum.localScale;
		creatureGameObject.transform.position = prototypeBumbletum.position;
		creatureGameObject.transform.rotation = prototypeBumbletum.rotation;

		// Remove its collider(s?)
		Destroy(creatureGameObject.GetComponent<Collider>());
		Destroy(creatureGameObject.GetComponent<CapsuleCollider>());
		Destroy(creatureGameObject.GetComponent<CreatureTapHandler>());

		//Parent the spinner to the creature
		spinner.transform.parent = creatureGameObject.transform;

		// Display the food
		UpdateFood ();

		// Wait a frame, so our info is loaded
		yield return null;

		// Find out chance values
		SetChanceValues();

		// TODO: Remove this
		TEMP_UpdateChanceText ();

		// Update the Spinner
		FindObjectOfType<SpinFiller>().UpdateSpinner();

		// Wait 2 seconds (For animation to end)
		yield return new WaitForSeconds (1.9f);

		// Play spin out animation
		introAnimation.SetTrigger("SlideOut");

		// TODO: Play ready to spin sound

		// Wait 2 seconds (For animation to end)
		yield return new WaitForSeconds (2.0f);

		// Unpause
		GameManager.instance.Paused = false;

	}


	void Update () {
		// Update temp text, this will go away at launch
		TEMP_UpdateChanceText ();
	}

	#endregion


	// Call this when the spinner finishes!
	public void OnSpinComplete () {
		if (spinner.GetCurrentPercent () <= catchChance) {
			StartCoroutine (CatchSuccess ());
		} else if (spinner.GetCurrentPercent () <= catchChance + runChance) {
			StartCoroutine (CatchFail ());
		} else {
			StartCoroutine (CatchTryAgain ());
		}
	}

	public IEnumerator CatchTryAgain () {

		GameManager.instance.isPaused = true;

		// Show Try Again animation
		tryAgainAnimation.SetTrigger("SlideIn");

		// Wait for it to end (with 1 second display overlap)
		yield return new WaitForSeconds(3.0f);

		// Show Try Again animation
		tryAgainAnimation.SetTrigger("SlideOut");

		// Wait for it to end
		yield return new WaitForSeconds(1.75f);

		// TODO: Play ready to spin sound

		// Reset Spinner
		GameManager.instance.isPaused = false;

	}

	public IEnumerator CatchFail () {
		GameManager.instance.isPaused = true;

		// Show escape particles
		Instantiate(DataManager.instance.failParticlePrefab, creatureGameObject.transform.position + DataManager.instance.particleOffset, creatureGameObject.transform.rotation);

		// Show fail animation
		runawayAnimation.SetTrigger("SlideIn");

		// Wait for it to end 
		yield return new WaitForSeconds(2.0f);

		// Add to graveyard
		GameManager.instance.playerData.graveyard.Add(GameManager.instance.selectedCreature.UID);

		// Wait for save of player status (to save graveyard)
		yield return StartCoroutine (DataManager.instance.DoPlayerSave ());
		yield return StartCoroutine (DataManager.instance.DoPlayerLoad ());

		// Fail Animation ends
		runawayAnimation.SetTrigger("SlideOut");

		// Wait for it to end 
		yield return new WaitForSeconds(2.0f);

		// Fade to black
		yield return StartCoroutine (GameManager.instance.FadeToBlack ());

		// Unpause (just controls)
		GameManager.instance.isPaused = false;

		// Return to Main
		ReturnToMain();
	}

	public IEnumerator CatchSuccess () {
		GameManager.instance.isPaused = true;

		// Show success animation
		catchAnimation.SetTrigger("SlideIn");

		// Show catch particles
		Instantiate(DataManager.instance.successParticlePrefab, creatureGameObject.transform.position + DataManager.instance.particleOffset, creatureGameObject.transform.rotation);

		// Wait for it to end
		yield return new WaitForSeconds(2.0f);

		// Add to graveyard -- this way, even if they lose it from their inventory, it still won't draw in the world again!
		GameManager.instance.playerData.graveyard.Add(GameManager.instance.selectedCreature.UID);

		// Add to inventory
		GameManager.instance.playerData.creatureInv.AddItem(GameManager.instance.selectedCreature);

		// Wait for our saves
		yield return StartCoroutine (DataManager.instance.DoPlayerSave ());
		yield return StartCoroutine (DataManager.instance.DoPlayerLoad ());

		// Show success animation
		catchAnimation.SetTrigger("SlideOut");

		// Wait for it to end
		yield return new WaitForSeconds(2.0f);

		// Fade to black
		yield return StartCoroutine (GameManager.instance.FadeToBlack ());

		// Unpause (just controls)
		GameManager.instance.isPaused = false;

		// Return to main
		ReturnToMain();
	}


	public void UpdateMode() {
		// If no parameter, Toggle and update
		UpdateMode (!isARMode);
	}

	public void UpdateMode (bool newIsARMode) {
		// Set to passed in value
		isARMode = newIsARMode;

		arImage.enabled = isARMode;
		gyroCamera.enabled = isARMode;
		gyroCamera.ResetTransform ();
		defaultWorld.SetActive (!isARMode);
	}
		
	public void UpdateChanceValues () {
		// If catch goes >1 , just make it 1
		if (catchChance > 1) {
			catchChance = 1;
		}

		// If catch goes <.01, just make it 1%
		if (catchChance < 0.01f) {
			catchChance = 0.01f;
		}

		// If run <0, then make it 0
		if (runChance < 0) {
			runChance = 0;
		}

		// If catch and run combined are >1, then just make run the "rest" of the 1
		if (catchChance + runChance > 1) {
			runChance = 1 - catchChance;
		}

		// Try again chance is whatever is left
		tryAgainChance = 1-(catchChance+runChance);

		// Update the Spinner
		FindObjectOfType<SpinFiller>().UpdateSpinner();

		// Update temp text, this will go away at launch
		TEMP_UpdateChanceText ();
	}

	public void SetChanceValues () {
		ActorCreature creatureData = (ActorCreature)GameManager.instance.selectedCreature.sharedData;
		catchChance = creatureData.catchChance.value;
		runChance = creatureData.fleeChance.value;

		UpdateChanceValues ();
	}

	public void ShowEatParticles () {
		Instantiate (DataManager.instance.eatParticlePrefab, creatureGameObject.transform.position + DataManager.instance.particleOffset, Quaternion.identity);
	}

	public void TEMP_UpdateChanceText () {
		TEMP_ValueDisplayText.text = "Catch Chance: "+catchChance*100+"%\nRun Chance: "+runChance*100+"%\nTry Again Chance: "+(tryAgainChance)*100+"%\nCurrent Value = "+spinner.GetCurrentPercent()*100+"% : "+spinner.GetTargetPercent()*100+"%";
	}

    public void ReturnToMain()
    {
        Destroy(creatureGameObject);
		GameManager.instance.LoadScene("MainScene");
    }

	public void UpdateFood () {

		// If there are no foods left, turn off bar and skip this
		if (!foodController.CheckBarDisplay ()) {
			return;
		}

		Debug.Log ("Drawing new food");

		// Destroy what is at the position, now.
		Destroy(foodController.item);

		// Spawn the new food at that position
		InstanceFood newFood = GameManager.instance.playerData.foodInv.GetItem (foodController.foodIndex) as InstanceFood;
		if (newFood != null) {
			foodController.item = newFood.CreateActor (foodController.foodPosition, foodController.foodRotation, foodController.gameObject.transform);

			// Remove the FoodTapHandler
			Destroy(foodController.item.GetComponent<FoodTapHandler>());

			// Add the food feeder
			FeedOnTap fot = foodController.item.AddComponent<FeedOnTap>();
			fot.arMode = this;
		} else {
			foodController.foodIndex = 0;
			UpdateFood ();
		}

	}

}
