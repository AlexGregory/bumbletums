﻿using UnityEngine;
using System.Collections;

public class FoodAdvanceButton : MonoBehaviour {

	public FoodController fc;
	public ArMode arm;

	[Range(-1,1)] public int direction;

	// Update is called once per frame
	void OnMouseDown () {

		// Move (direction)
		fc.foodIndex += direction;

		// Loop
		if (fc.foodIndex >= GameManager.instance.playerData.foodInv.ItemCount) {
			fc.foodIndex = 0;
		} else if (fc.foodIndex < 0) {
			fc.foodIndex = GameManager.instance.playerData.foodInv.ItemCount-1;
		}

		// Update food display
		arm.UpdateFood ();
	}
}
