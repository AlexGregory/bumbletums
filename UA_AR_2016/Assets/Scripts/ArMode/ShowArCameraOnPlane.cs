﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// This is used on a Plane object that is placed directly in front of the camera with it's face towards the 
/// camera. This allows any 3D objects in front of the plane to cast a shadow onto the AR view.
/// 
/// Note: There are some problems with this script, such as improper skewing of the texture when the phone is rotated.
/// This script is here if we fix the problems and decide to use it later.
/// </summary>
public class ShowArCameraOnPlane : MonoBehaviour {
	#region Components

	private Transform tf;
	private Renderer rend;

	#endregion

	#region Private Variables

	private WebCamTexture mCamera = null; //The web cam's texture gathered from the device
	private Quaternion baseRotation; //The default rotation of the object at start
	private float forwardDistanceFromCamera; //The forward distance between the camera and the object

	#endregion

	#region MonoBehaviour Methods

	// Use this for initialization
	void Start () {
		//Get Components
		tf = GetComponent<Transform> ();
		rend = GetComponent<Renderer> ();

		//Start the device's camera
		mCamera = new WebCamTexture ();
		mCamera.Play ();

		//Set the mesh's main texture to the web cam's texture
		rend.material.mainTexture = mCamera;

		//Get the forward distance between the camera and the object
		forwardDistanceFromCamera = Vector3.Distance (Camera.main.transform.position, tf.position);

		//Rotate the object so the web cam's texture is properly displayed
		baseRotation = transform.rotation;
		tf.rotation = baseRotation * Quaternion.AngleAxis (mCamera.videoRotationAngle, Vector3.up);

		//Calculate the scale based on the camera's projection
		float height = 2 * Mathf.Tan (0.5f * Camera.main.fieldOfView * Mathf.Deg2Rad) * (forwardDistanceFromCamera/10);
		float width = height * (float)Screen.width / (float)Screen.height;
		tf.localScale = new Vector3 (height, 1, width);
	}

	// Update is called once per frame
	void Update () {
		tf.rotation = baseRotation * Quaternion.AngleAxis (mCamera.videoRotationAngle, Vector3.up);
	}

	#endregion
}
