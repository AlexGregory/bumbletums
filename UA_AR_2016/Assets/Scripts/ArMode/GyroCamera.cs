﻿using UnityEngine;
using System.Collections;

public class GyroCamera : MonoBehaviour {

	[SerializeField]
	private Camera cam; //The camera to rotate based on Gyro

	private Transform cameraTf; //The camera's transform

	// Use this for initialization
	void Awake() {
		cameraTf = cam.GetComponent<Transform> ();
	}

	void Start() {
		if (SystemInfo.supportsGyroscope) {
			Input.gyro.enabled = true; 
		} else {
			this.enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		cameraTf.Rotate(new Vector3(-Input.gyro.rotationRateUnbiased.x, -Input.gyro.rotationRateUnbiased.y, -Input.gyro.rotationRateUnbiased.z));	
	}

	public void ResetTransform() {
		cameraTf.rotation = Quaternion.identity;
	}

}
