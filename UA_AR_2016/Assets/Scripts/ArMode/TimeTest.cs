﻿using UnityEngine;
using System.Collections;
using System;

public class TimeTest : MonoBehaviour {

	public GameObject[] backgrounds;
	DateTime now;
	public int hour;

	// Use this for initialization
	void Start () {
		now = DateTime.Now;
		hour = now.Hour;

		// Turn off or on sprites based on time
		for (int spot = 0; spot < backgrounds.Length; spot++) {
			GameObject checkImage = backgrounds [spot];
			ImageStats currentStats = checkImage.GetComponent<ImageStats>();

			if (hour >= currentStats.startHour && hour < currentStats.endHour) {
				checkImage.SetActive (true);
			} else {
				checkImage.SetActive (false);
			}
		}
	}

	// Update is called once per frame
	void Update () {
	}
}