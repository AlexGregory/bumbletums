﻿using UnityEngine;
using System.Collections;

public class FoodController : MonoBehaviour {

	public GameObject item;
	public int foodIndex;
	public Vector3 foodPosition;
	public Quaternion foodRotation;

	void Start () {

		// Check if we should display the bar at all
		CheckBarDisplay ();

		// Delete what is at itemPosition now - but keep the location
		foodPosition = item.transform.position;
		foodRotation = item.transform.rotation;

		// Start with our index at 0, and create the food at this item
		foodIndex = 0;
	}

	public bool CheckBarDisplay () {
		// If we have no food, this whole bar is disabled
		if (GameManager.instance.playerData.foodInv.ItemCount <= 0) {
			gameObject.SetActive (false);
			return false;
		} else {
			// Otherwise it is active. 
			gameObject.SetActive (true);
			return true;
		}
	}

}
