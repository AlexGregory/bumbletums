﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// This is used on a UI component that has a RawImage. It will apply the device's web cam texture to the UI element.
/// Unlike SowArCameraOnPlane it does not suffer from improper skewing of the image when the phone is turned. However, there
/// is no shadows. 
/// 
/// Note: Unsure if the raw image scales correctly on every device. It still may need some tweaking.
/// </summary>
[RequireComponent(typeof(RawImage))]
[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(AspectRatioFitter))]
public class ShowArCameraUI : MonoBehaviour {

	#region Inspector Variables

	[SerializeField]
	private int initialCameraId = 0;

	#endregion

	#region Components

	private RawImage image;
	private RectTransform rectTransform;
	private AspectRatioFitter imageFitter;

	#endregion

	#region Private Variables

	//The different cameras on the device
	private WebCamDevice frontCameraDevice;
	private WebCamDevice backCameraDevice;
	private WebCamDevice activeCameraDevice;

	//The textures generated from each of the device's cameras
	private WebCamTexture frontCameraTexture;
	private WebCamTexture backCameraTexture;
	private WebCamTexture activeCameraTexture;

	//The image rotation
	private Vector3 rotationVector;

	//Image UV Rects
	private Rect defaultRect = new Rect(0f, 0f, 1f, 1f); //Default orientation
	private Rect fixedRect = new Rect(0f, 0f, 1f, -1f); //Vertically flipped

	//Rect Transform Scales
	private Vector3 standardScale;
	private Vector3 defaultScale = new Vector3(1f, 1f, 1f);
	private Vector3 fixedScale = new Vector3(-1f, 1f, 1f);

	#endregion

	#region MonoBehaviour Methods

	// Use this for initialization
	void Start () {
		//Get the Components
		image = GetComponent<RawImage> ();
		rectTransform = GetComponent<RectTransform> ();
		imageFitter = GetComponent<AspectRatioFitter> ();

		//Check if the device has cameras
		bool noDevices = WebCamTexture.devices.Length == 0;
		if (noDevices) {
			Debug.Log ("This device has no cameras!");
			Destroy (this); //Remove this script
		} else {
			//Grab the first web camera and it's texture
			//The devices array can be used to grab the other camera.
			frontCameraDevice = WebCamTexture.devices [initialCameraId];
			frontCameraTexture = new WebCamTexture (frontCameraDevice.name);

			//Set the camera filter modes so that it produces a smoother looking image
			frontCameraTexture.filterMode = FilterMode.Trilinear;

			//Set the front camera as the default camera
			SetActiveCamera(frontCameraDevice);
		}
	}
	
	// Update is called once per frame
	void Update () {
		//Skip making image adjustments if the size data is incorrect
		//This needs to run every frame since unity does not provide
		//correct size data as soon as a device camera is started.
		//If one is switched and turned on this needs to be here to ensure accuracy.
		if (activeCameraTexture.width < 100) {
			Debug.Log ("Still waiting another frame for correct info...");
		} else {
			//This will rotate the image to show correct orientation
			//The video rotation angle will change depending on the device's texture
			rotationVector.z = -activeCameraTexture.videoRotationAngle;
			image.rectTransform.localEulerAngles = rotationVector;

			//Set the image fitter's ratio
			float videoRatio = (float) activeCameraTexture.width / (float) activeCameraTexture.height;
			imageFitter.aspectRatio = videoRatio;

			//Unflip the image if it is vertically flipped
			image.uvRect = activeCameraTexture.videoVerticallyMirrored ? fixedRect : defaultRect;

			//Mirror front facing camera's image horizontally to look more natural
			rectTransform.localScale = (activeCameraDevice.isFrontFacing ? fixedScale : defaultScale) * imageFitter.aspectRatio;
		}
	}

	void OnDestroy() {
		//Stop the current webcam
		if (activeCameraTexture != null) {
			activeCameraTexture.Stop ();
		}
	}

	#endregion

	#region Private Methods

	private void SetActiveCamera(WebCamDevice webCam) {
		//Stop the old camera texture before starting a new one
		if (activeCameraTexture != null) {
			activeCameraTexture.Stop ();
		}

		//Set the active camera to the new one
		activeCameraDevice = webCam;
		activeCameraTexture = new WebCamTexture(webCam.name, Screen.height, Screen.width);

		//Set the raw image textures to the active camera's texture
		image.texture = activeCameraTexture;
		image.material.mainTexture = activeCameraTexture;

		//Start recording the activate camera texture
		activeCameraTexture.Play ();
	}

	#endregion
}
