﻿using UnityEngine;
using System.Collections;

public class PropStats : MonoBehaviour {

	public GameObject[] sceneProps;		//array of props for the given scene
	public GameObject[] spawnPoints;	//an array of spawnpoints where objects will be held

	public Transform tf;
	Vector3 correctLocation;

	public GameObject spawnedProp;
	GameObject pickedProp, newPickedProp;
	Transform currentChild;

	public ObjectStats stat;

	void Start(){


		tf = GetComponent<Transform> ();

		//TODO:  Change the tag to match the general tag plus "SpawnPoint"
		//Debug.Log(this.name);
		spawnPoints = GameObject.FindGameObjectsWithTag (this.name + "SpawnPoint");

		for (int spot = 0; spot < spawnPoints.Length; spot++) {
			correctLocation = new Vector3 (spawnPoints[spot].transform.position.x + 2, spawnPoints[spot].transform.position.y, spawnPoints[spot].transform.position.z);

			//Debug.Log (GetRandomProp());
			pickedProp = GetRandomProp();
			//Debug.Log ("This is the grass prop" + pickedProp);
			//stat.timesSpawned = 1;
			spawnedProp = Instantiate (pickedProp, correctLocation, Quaternion.identity) as GameObject;
			spawnedProp.transform.parent = spawnPoints [spot].transform;
			spawnedProp.layer = 8;
			stat = spawnedProp.GetComponent<ObjectStats> ();

			stat.timesSpawned = 1;
		}

		//correctLocation = new Vector3 (tf.position.x + 2, tf.position.y, tf.position.z);


	}

	public GameObject GetRandomProp(){
		
		pickedProp = sceneProps [UnityEngine.Random.Range (0, sceneProps.Length)];
		stat = pickedProp.GetComponent<ObjectStats> ();


		if (stat.canHaveMultiple == false) {

			for (int spot = 0; spot < spawnPoints.Length; spot++) {
			
				if (spawnPoints [spot].transform.childCount > 0) {
					//Debug.Log ("You have children!");
					currentChild = spawnPoints [spot].gameObject.transform.GetChild (0);
					//Debug.Log (currentChild.name);
					//Debug.Log ((pickedProp.name + "(Clone)") + " == " + currentChild.name);
					if ((pickedProp.name + "(Clone)") == currentChild.name) {
						//Debug.Log ("Its a match!");
						newPickedProp = sceneProps [UnityEngine.Random.Range (0, sceneProps.Length)];
						stat = newPickedProp.GetComponent<ObjectStats> ();
						//Debug.Log ("This is the new picked object" + newPickedProp);


						//TODO: Now got to check this one.
						while ((newPickedProp.name + "(Clone)") == currentChild.name) {

							for (int spot2 = 0; spot2 < spawnPoints.Length; spot2++) {

								if (spawnPoints [spot2].transform.childCount > 0) {
									//Debug.Log ("You have children!");
									currentChild = spawnPoints [spot2].gameObject.transform.GetChild (0);
									//Debug.Log (currentChild.name);
									//Debug.Log ((pickedProp.name + "(Clone)") + " == " + currentChild.name);
									if ((newPickedProp.name + "(Clone)") == currentChild.name) {
										//Debug.Log ("Its a match!");
										newPickedProp = sceneProps [UnityEngine.Random.Range (0, sceneProps.Length)];
										stat = newPickedProp.GetComponent<ObjectStats> ();
										//Debug.Log ("This is the new picked object" + newPickedProp);

									}
								}
							}
						}
						pickedProp = newPickedProp;
					}
				}
			}


			}
			
		return pickedProp;

	}

}
