﻿using UnityEngine;
using System.Collections;


public delegate void MouseMoved(float xMovement, float yMovement);
public class InputManager : MonoBehaviour
{

    #region Private References
    private float _xMovement;
    private float _yMovement;

	private Vector2 prevMousePostion;
    #endregion

    #region Events
    public static event MouseMoved MouseMoved;
    #endregion

    #region Event Invoker Methods
    private static void OnMouseMoved(float xmovement, float ymovement)
    {
        var handler = MouseMoved;
        if (handler != null) handler(xmovement, ymovement);
    }
    #endregion


    #region Private Methods
    private void InvokeActionOnInput()
    {

		if (Input.touchSupported) {
			if (Input.touches[0].phase == TouchPhase.Moved) {
				_xMovement = Input.touches [0].deltaPosition.x;
				_yMovement = Input.touches [0].deltaPosition.y;
				OnMouseMoved (_xMovement, _yMovement);
			}
		} else {
			if (Input.GetMouseButtonDown(0)) { 
				prevMousePostion = Input.mousePosition;
			}

			if (Input.GetMouseButton (0)) {
				_xMovement = Input.mousePosition.x - prevMousePostion.x;
				_yMovement = Input.mousePosition.y - prevMousePostion.y;

				OnMouseMoved (_xMovement, _yMovement);

				prevMousePostion = Input.mousePosition;
			}
		}
    }
    #endregion

    #region Unity CallBacks

	void Start () {
		prevMousePostion = Input.mousePosition;
	}

	void Update () {
        InvokeActionOnInput();
	}

    #endregion
}
