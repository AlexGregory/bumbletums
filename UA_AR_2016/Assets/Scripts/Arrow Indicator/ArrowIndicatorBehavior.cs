﻿using UnityEngine;
using System.Collections;

public class ArrowIndicatorBehavior : MonoBehaviour {

    #region Public Variables

    [Range(1, 10)]
    public float positionOffset = 2;
	public float opacity;

    public bool isVisible;
	public GameObject targetObject;
    
    #endregion

    #region Private Variables

	public GameObject playerObject;
	private GameObject targetObjectWithOffset;
    private Vector3 targetSpawnPoint;
	private Vector3 originToTarget;
    
    #endregion

    #region Unity Callbacks
    void Start () {
	}

	void Update () {
	}



    #endregion
}
