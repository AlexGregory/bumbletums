﻿using UnityEngine;
using System.Collections;

public class ArrowIndicatorManager : MonoBehaviour {

    #region Public Variables

	[Header("GameObjects")] 
	public WorldDrawer worldDrawer;
    public GameObject IndicatorPrefab;

	[Header("Display Variables")] 
	public Vector3 arrowOffset;
	public float maxDistance;
	public float minDistance;
	public float minAlpha;
	public float maxAlpha;

	[Header("Arrays")] 
	[SerializeField]
	public ActorCreature[] bumbleTumObjects;
	[SerializeField]
    public Transform[] locations;
	[SerializeField]
    public GameObject[] arrowIndicators;

    #endregion

    #region Unity Callbacks

    void Start () {
	}

	public void ClearArrows() {
		for (int i = 0; i < arrowIndicators.Length; i++) {
			Destroy (arrowIndicators [i]);
		}
	}


	public void CreateArrows () {
		ClearArrows ();

		// Find all the ActorCreatures and use them to resize the other arrays
		bumbleTumObjects = FindObjectsOfType<ActorCreature>();
		locations = new Transform[bumbleTumObjects.Length];
		arrowIndicators = new GameObject[bumbleTumObjects.Length];

		// For each ActorCreature...
		for (int i = 0; i < bumbleTumObjects.Length; i++)
		{
			// Save its location
			locations[i] = bumbleTumObjects[i].transform;

			// And create an arrow indicator for it
			GameObject arrowIndic = Instantiate(IndicatorPrefab, worldDrawer.worldPlayerVisuals.transform.position, Quaternion.identity) as GameObject;
			arrowIndicators[i] = arrowIndic;
			arrowIndicators[i].transform.parent = worldDrawer.worldPlayerVisuals.transform;

			//Set the position of the indicator arrow on the player, then offset ALL OF ITS CHILDREN it so that it "floats" around the parent
			foreach (Transform child in arrowIndicators[i].transform) {
				child.transform.position = arrowOffset;
			}

			// Make it a child of the player, so it would move with them (if they moved)
			arrowIndicators[i].transform.parent = worldDrawer.worldPlayerVisuals.transform;
		}
	}

	public void UpdateArrows () {
		for (int i =0; i < arrowIndicators.Length; i++) {

			//This is a modifier so that the arrow's don't point to a new y position. They'll always point perpendicular to the parent object.
			Vector3 modifiedTargetPosition = new Vector3 ( bumbleTumObjects[i].transform.position.x, arrowIndicators[i].transform.position.y, bumbleTumObjects[i].transform.position.z);

			//Vector Math for pointing the arrow towards the target Bumbletum
			//Vector3 originToTarget = modifiedTargetPosition - worldDrawer.worldPlayerVisuals.transform.position;

			//Point to object
			arrowIndicators[i].transform.LookAt (modifiedTargetPosition);

			// Find distance
			float distanceFromBumbletum = Vector3.Distance (modifiedTargetPosition, arrowIndicators[i].transform.position);

			// Find opacity, based on distance
			float opacity = ((distanceFromBumbletum * (minAlpha-maxAlpha))/(minDistance-maxDistance)) ;
			opacity = 1 - Mathf.Clamp (opacity, 0f, 1f);

			// if out of range, set opacity to 0
			if (distanceFromBumbletum < minDistance || distanceFromBumbletum > maxDistance) {
				opacity = 0;			
			} 

			// Set opacity for each arrow
			Renderer[] arrowRenderers= arrowIndicators[i].GetComponentsInChildren<Renderer>();
			foreach (Renderer r in arrowRenderers) {
				
				if (opacity <= 0) {
					r.enabled = false;
				} else {
					r.enabled = true;
				}

				r.material.color = new Color (r.material.color.r, r.material.color.g, r.material.color.b, opacity);
			}
		}

	}


    void Update () {
	}
   
    #endregion
}



